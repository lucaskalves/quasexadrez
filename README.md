#QuaseXadrez

Um jogo de xadrez simplificado: somente existem peões e cavalos.
Também pode ser chamado de Batalha de Peões.

## Dependências

Esse aplicativo utiliza o Qt 5 e suporta o Qt 4. É necessário que ele
esteja instalado em seu sistema.

## Execução

O código foi implementado e testado em ambiente Linux. A aplicação funciona
com o Qt4 e Qt5. No Ubuntu, o Qt já está instalado.

Para executá-lo, execute o programa "quasexadrez" presente na pasta *bin*.

## Compilação

Esse software usa o framework Qt. Portanto, nunca mexa no Makefile!
A rotina de compilação é controlada pelo *qmake*, um programa disponível
pelo Qt. O arquivo de configuração da compilação é o *quasexadrez.pro*.

1. `qmake`
2. `make`
3. Para executar: `bin/quasexadrez`

## Compilação dos Testes

Esse software usa o Googletest para a realização dos testes unitários. Para
compilar os testes, você precisa tem duas alternativas.

### Através do CMake

Para compilar utilizando o CMake, rode os seguintes comandos:

1. `cd src/tests/cmake/build`
2. `cmake ..`
3. `make`
4. Para executar: `./alltests`

### Através do Make

Para compilar utilizando o Make, na primeira vez você precisará compilar o GTest
presente na pasta gtest-1.6.0. Para isso rode os seguintes comandos:

1. `cd src/tests/gtest-1.6.0`
2. `./configure`
3. `cd ./make`
4. `make`

Após a compilação do GTest, você pode compilar os testes unitários utilizando o
Makefile presente na pasta *tests*. Para isso, rode:

1. `cd src/tests`
2. `make`
3. Para executar: `bin/alltests`

## Autores

Lucas Kreutz (lucas@lucaskreutz.com.br)   
Rodrigo Zanella

## Licen�a

A boa e velha licen�a MIT. Leia o arquivo license.txt para maiores informa��es.
