TEMPLATE = app

HEADERS = src/mainwindow.h \
          src/casa.h \
          src/peca.h \
          src/peao.h \
          src/torre.h \
          src/posicao.h \
          src/tabuleiro.h \
          src/movimento.h \
          src/partida.h \
          src/jogador.h \
          src/jogadorhumano.h \
          src/jogadormaquina.h

SOURCES = src/mainwindow.cpp \
          src/main.cpp \
          src/casa.cpp \
          src/peca.cpp \
          src/peao.cpp \
          src/torre.cpp \
          src/posicao.cpp \
          src/tabuleiro.cpp \
          src/movimento.cpp \
          src/partida.cpp \
          src/jogador.cpp \
          src/jogadorhumano.cpp \
          src/jogadormaquina.cpp

FORMS += ui/mainwindow.ui 
RESOURCES += images.qrc

# Qt5 requires this...
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++0x

DESTDIR = ./bin
OBJECTS_DIR = ./bin
MOC_DIR = ./moc
UI_DIR = ./ui
RCC_DIR = ./rcc
