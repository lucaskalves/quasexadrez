#include "casa.h"
#include "peao.h"
#include "torre.h"
#include <stdio.h>

Casa::Casa(const Casa &c) {
  m_EstaOcupada = c.estaOcupada();
  m_CorDaCasa = c.corDaCasa();
  if(c.estaOcupada()) {
      if(c.peca()->tipo() == PEAO){
          m_Peca = std::make_shared<Peao>(c.peca()->cor(), c.peca()->posicao());
      } else {
          m_Peca = std::make_shared<Torre>(c.peca()->cor(), c.peca()->posicao());
      }
  } else {
      m_Peca = 0;
  }
}

Casa::Casa(CorCasa c) {
  m_EstaOcupada = false;
  m_Peca = 0;
  m_CorDaCasa = c;
}

bool Casa::equal(Casa& o) {
  bool mesmoEstaOcupada = m_EstaOcupada == o.estaOcupada();
  bool mesmaCor = m_CorDaCasa == o.corDaCasa();

  bool mesmaPeca = false;
  if(m_EstaOcupada && o.estaOcupada()) {
    mesmaPeca = m_Peca->equal(*(o.peca()));
  } else if(!m_EstaOcupada && !o.estaOcupada()) {
    // se nenhuma das duas estiver ocupada, a peca e a mesma
    mesmaPeca = true;
  }

  return mesmoEstaOcupada && mesmaCor && mesmaPeca;
}

CorCasa Casa::corDaCasa() const {
  return m_CorDaCasa;
}

std::shared_ptr<Peca> Casa::peca() const {
  return m_Peca;
}

bool Casa::estaOcupada() const {
  return m_EstaOcupada;
}

bool Casa::estaLivre() const {
  return !m_EstaOcupada;
}

void Casa::ocupar(std::shared_ptr<Peca> p) {
  m_EstaOcupada = true;
  m_Peca = p;
}

void Casa::liberar() {
  m_EstaOcupada = false;
  m_Peca = 0;
}
