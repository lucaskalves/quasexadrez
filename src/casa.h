#ifndef CASA_H
#define CASA_H

#include "peca.h"
#include <memory>

enum CorCasa {CASABRANCA, CASAPRETA};

class Peca;

class Casa
{
private:
  std::shared_ptr<Peca> m_Peca;
  bool m_EstaOcupada;
  CorCasa m_CorDaCasa;

public:
    Casa(const Casa &c);
    Casa(CorCasa c);

    CorCasa corDaCasa() const;
    std::shared_ptr<Peca> peca() const;
    bool estaOcupada() const;
    bool estaLivre() const;
    void ocupar(std::shared_ptr<Peca> p);
    void liberar();
    bool equal(Casa&);
};

#endif
