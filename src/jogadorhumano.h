#ifndef JOGADORHUMANO_H
#define JOGADORHUMANO_H

#include "jogador.h"

class JogadorHumano: public Jogador {
  Cor m_Cor;

public:
  JogadorHumano();
  ~JogadorHumano();
  void definirCor(Cor c);
  Cor cor();
  Movimento fazerJogada(Tabuleiro &t);
};

#endif