#include "posicao.h"
#include <iostream>

Posicao::Posicao() : m_Linha(0), m_Coluna(0) {}

Posicao::Posicao(int l, int c) : m_Linha(l), m_Coluna(c) {}

int Posicao::linha() const {
  return m_Linha;
}

int Posicao::coluna() const {
  return m_Coluna;
}

bool Posicao::equal(Posicao o) {
  return m_Linha == o.linha() && m_Coluna == o.coluna();
}

std::ostream& operator<<(std::ostream& os, const Posicao& posicao) {
  os << "(" << posicao.linha()
            << "," << posicao.coluna()
            << ")";
  return os;
}