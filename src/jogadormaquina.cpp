#include "jogadormaquina.h"
#include <iostream>
#include <vector>
#include <limits>
#include <time.h>

JogadorMaquina::JogadorMaquina(int profundidadeMaxima) {
  m_ProfundidadeMaxima = profundidadeMaxima;
}

JogadorMaquina::~JogadorMaquina() {

}

void JogadorMaquina::definirCor(Cor c) {
  m_Cor = c;
}


Cor JogadorMaquina::cor() {
  return m_Cor;
}
void JogadorMaquina::defineProfundidade(int p){
    m_ProfundidadeMaxima = p;
}

/**
* Calcula a melhor jogada que a maquina pode realizar
*/
Movimento JogadorMaquina::fazerJogada(Tabuleiro &t) {
  int max = std::numeric_limits<int>::max();
  int min = std::numeric_limits<int>::min();
  int valorEscolhido = 0;

  time_t timer1;
  time_t timer2;
  time(&timer1);

  std::cout << "comecei a pensar " << std::endl;
  valorEscolhido = negamax(t, m_ProfundidadeMaxima, min, max, 1);
  time(&timer2);
  std::cout << "parei de pensar " << timer2-timer1 << "s";
  std::cout << " valor escolhido: " << valorEscolhido << std::endl;

  if(!t.fimDeJogo()) {
    return melhorMovimento;
  }

  throw 2;
}

/**
* Implementacao do negamax para calcular melhor valor. Salva o melhor movimento encontrado
* no atributo melhorMovimento.
*/
int JogadorMaquina::negamax(Tabuleiro &estado, int profundidade, int alfa, int beta, int maximizar) {

  Cor outraCor = (m_Cor == BRANCA) ? PRETA : BRANCA;
  Cor cor = maximizar > 0 ? m_Cor : outraCor;
  bool debug = false;

  if (profundidade == 0 || estado.fimDeJogo()) {
    Cor corAnterior = maximizar > 0 ? outraCor : m_Cor;
    return maximizar * estado.valorHeuristica(corAnterior);
  } else {
    std::vector<Movimento> movimentos = estado.gerarMovimentos(cor);

    for (std::vector<Movimento>::iterator it = movimentos.begin(); it != movimentos.end(); ++it) {
      Movimento movimento = *it;

      estado.mover(movimento);
      int valor = -negamax(estado, profundidade-1, -beta, -alfa, -maximizar);
      estado.desmover();

      if(debug) {
        // imprime arvore do negamax
        for(int t = profundidade; t>0;t--) {
          std::cout<<"\t";
        }
        std::cout<< "("<<profundidade<<") mov " << (cor == BRANCA ? "branca" : "preta");
        std::cout<< movimento;
        std::cout<< " :: " << valor << std::endl;
      }

      if(valor >= beta) {
        if(profundidade == m_ProfundidadeMaxima) {
          melhorMovimento = movimento;
        }
        return valor;
      }
      if(valor > alfa) {
        if(profundidade == m_ProfundidadeMaxima) {
          melhorMovimento = movimento;
        }
        alfa = valor;
      }
    }

    return alfa;
  }
}
