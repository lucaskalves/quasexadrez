#ifndef TORRE_H
#define TORRE_H

#include <string>
#include "peca.h"
#include "posicao.h"
#include "tabuleiro.h"
#include "movimento.h"

class Torre: public Peca
{
public:
  Torre(Cor cor, Posicao p);
  void moverParaPosicao(Posicao p);
  bool movimentoValido(Posicao posicaoNova, Tabuleiro *t);
  vector<Movimento>& gerarMovimentos(Tabuleiro *t);
};

#endif