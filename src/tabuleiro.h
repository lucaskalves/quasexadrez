#ifndef TABULEIRO_H
#define TABULEIRO_H

#include "posicao.h"
#include "casa.h"
#include "movimento.h"
#include "peca.h"
#include <memory>
#include <vector>
#include <stack>

class Casa;
class Peca;

using std::vector;
using std::stack;

class Tabuleiro {
  Movimento m_UltimoMov;
  bool m_HouveJogada;
  std::shared_ptr<Peca> m_PecaMovida;
  int m_NumTorresBrancas;
  int m_NumTorresPretas;
  int m_NumPeoesBrancos;
  int m_NumPeoesPretos;
  stack<std::shared_ptr<Peca>> m_PecasMortas;
  stack<Movimento> m_MovimentosRealizados;
  vector<Movimento> m_Movimentos;
  bool m_GerouMovimentos;
  Cor m_CorVencedora;
  void inicializar();
  void matarPeca(Posicao &pos);

public:
  static const int LINHAS = 8;
  static const int COLUNAS = 8;
  
  Tabuleiro();
  Tabuleiro(const Tabuleiro &outro);
  ~Tabuleiro();

  void montarTabuleiroInicial();
  int numTorresBrancas() const;
  int numTorresPretas() const;
  int numPeoesBrancos() const;
  int numPeoesPretos() const;
  Cor corVencedora() const;
  bool posicaoLivre(Posicao p);
  std::shared_ptr<Peca> pecaEmPosicao(Posicao p);
  std::shared_ptr<Casa> casaDaPosicao(Posicao p) const;
  void mover(Movimento m);
  void desmover();
  Movimento ultimoMovimento() const;
  bool houveJogada() const;
  std::shared_ptr<Peca> pecaMovida() const;
  static bool posicaoValida(Posicao p);
  bool equal(Tabuleiro& o);
  int valorHeuristica(Cor quemJoga);
  vector<Movimento> gerarMovimentos(Cor quemJoga);
  bool fimDeJogo();

private:
   std::shared_ptr<Casa> m_MapaDeCasas[LINHAS][COLUNAS];
};

#endif
