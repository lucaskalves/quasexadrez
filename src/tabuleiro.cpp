#include "tabuleiro.h"
#include "torre.h"
#include "peao.h"
#include <iostream>


Tabuleiro::Tabuleiro() {
  m_HouveJogada = false;
  m_CorVencedora = BRANCA;
  inicializar();
}

// copy constructor -> copia o tabuleiro atual
Tabuleiro::Tabuleiro(const Tabuleiro &outro){
  int i,j;

  // copia os atributos
  m_UltimoMov = outro.ultimoMovimento();
  m_HouveJogada = outro.houveJogada();
  m_NumTorresBrancas = outro.numTorresBrancas();
  m_NumTorresPretas = outro.numTorresPretas();
  m_NumPeoesBrancos = outro.numPeoesBrancos();
  m_NumPeoesPretos = outro.numPeoesPretos();

  // copia a peca movida se houve movimento
  if(outro.houveJogada()) {
    Cor cor = outro.pecaMovida()->cor();
    Posicao pos = outro.pecaMovida()->posicao();

    if(outro.pecaMovida()->tipo() == PEAO){
      m_PecaMovida = std::make_shared<Peao>(cor, pos);
    } else {
      m_PecaMovida = std::make_shared<Torre>(cor, pos);
    }
  }

  // copia as casas
  for(i=0; i<LINHAS; i++) {
    for(j=0; j<COLUNAS; j++) {
      Casa casa = *(outro.casaDaPosicao(Posicao(i,j)));
      m_MapaDeCasas[i][j] = std::make_shared<Casa>( casa );
    }
  }
}

Tabuleiro::~Tabuleiro() {
}

// aloca as casas para o tabuleiro
void Tabuleiro::inicializar() {
  int i=0, j=0;

  // inicializa todas as casas necessarias
  for(i=0; i<LINHAS; i++) {
    for(j=0; j<COLUNAS; j++) {
      CorCasa corDaCasa = ((i+j) % 2 == 0) ? CASABRANCA : CASAPRETA;
      m_MapaDeCasas[i][j] = std::make_shared<Casa>(corDaCasa);
    }
  }
}

// monta o tabuleiro inicial
void Tabuleiro::montarTabuleiroInicial() {
  m_NumTorresBrancas = 2;
  m_NumTorresPretas = 2;
  m_NumPeoesBrancos = 8;
  m_NumPeoesPretos = 8;

  // coloca as torres pretas
  m_MapaDeCasas[0][0]->ocupar(std::make_shared<Torre>(PRETA, Posicao(0,0)));
  m_MapaDeCasas[0][7]->ocupar(std::make_shared<Torre>(PRETA, Posicao(0,7)));

  // coloca as torres brancas
  m_MapaDeCasas[7][0]->ocupar(std::make_shared<Torre>(BRANCA, Posicao(7,0)));
  m_MapaDeCasas[7][7]->ocupar(std::make_shared<Torre>(BRANCA, Posicao(7,7)));

  for(int j=0; j<COLUNAS; j++) {
    // coloca os peoes pretos
    m_MapaDeCasas[1][j]->ocupar(std::make_shared<Peao>(PRETA, Posicao(1,j)));
    // coloca os peoes brancos
    m_MapaDeCasas[6][j]->ocupar(std::make_shared<Peao>(BRANCA, Posicao(6,j)));
  }
}

int Tabuleiro::numTorresBrancas() const {
  return m_NumTorresBrancas;
}
int Tabuleiro::numTorresPretas() const {
  return m_NumTorresPretas;
}
int Tabuleiro::numPeoesBrancos() const {
  return m_NumPeoesBrancos;
}
int Tabuleiro::numPeoesPretos() const {
  return m_NumPeoesPretos;
}

Cor Tabuleiro::corVencedora() const {
  return m_CorVencedora;
}

bool Tabuleiro::posicaoLivre(Posicao p) {
  return casaDaPosicao(p)->estaLivre();
}

std::shared_ptr<Peca> Tabuleiro::pecaEmPosicao(Posicao p) {
  return casaDaPosicao(p)->peca();
}

std::shared_ptr<Casa> Tabuleiro::casaDaPosicao(Posicao p) const {
  if(!posicaoValida(p)) {
    throw 3;
  }

  return m_MapaDeCasas[p.linha()][p.coluna()];
}

/**
* Mata a peca que se encontra na posição pos.
*/
void Tabuleiro::matarPeca(Posicao &pos) {
  if(casaDaPosicao(pos)->estaOcupada()) {
    Cor corDaPeca = casaDaPosicao(pos)->peca()->cor();
    Tipo tipoDaPeca = casaDaPosicao(pos)->peca()->tipo();
    m_PecasMortas.push(casaDaPosicao(pos)->peca());

    // decrementa os contadores de peca
    if(corDaPeca == BRANCA) {

      if(tipoDaPeca == PEAO) {
        m_NumPeoesBrancos -= 1;
      } else if( tipoDaPeca == TORRE) {
        m_NumTorresBrancas -= 1;
      }

    } else {

      if(tipoDaPeca == PEAO) {
        m_NumPeoesPretos -= 1;
      } else if( tipoDaPeca == TORRE) {
        m_NumTorresPretas -= 1;
      }

    }

    // libera a casa que ocupava
    casaDaPosicao(pos)->liberar();
  } else {
    throw 4;
  }
}

/*
* Move uma peca entre casas do tabuleiro.
*
* Esse metodo assume que o movimento ja foi validado para o jogo, ou seja, que
* obedece as regras do jogo.
* As checagens de ocupacao sao feitas meramente para evitar problemas com
* ponteiros, nao para confirmar a validade da jogada.
*/
void Tabuleiro::mover(Movimento m) {

  // so faz movimento se algo realmente se mover
  if(!m.posicaoInicial().equal(m.posicaoFinal())) {
    // se a posicao inicial tiver peca
    if(casaDaPosicao(m.posicaoInicial())->estaOcupada()) {

      // se a posicao destino estiver ocupada
      if(casaDaPosicao(m.posicaoFinal())->estaOcupada()) {
        // mata a peca
        Posicao posVitima = m.posicaoFinal();
        matarPeca(posVitima);

      } else if(casaDaPosicao(m.posicaoInicial())->peca()->tipo() == PEAO) {

        // verifica  en passant ja que peao ta se movendo para casa vazia
        int deltaLinha = abs(m.posicaoInicial().linha() - m.posicaoFinal().linha());
        int deltaColuna = abs(m.posicaoInicial().coluna() - m.posicaoFinal().coluna());
        if(deltaLinha == 1 && deltaColuna == 1) {

          // ja que peao se moveu na diagonal para casa vazia, so pode ser en passant
          Posicao posInimigo = Posicao(m.posicaoInicial().linha(), m.posicaoFinal().coluna());
          if(casaDaPosicao(posInimigo)->estaOcupada()) {
            // mata a peca
            matarPeca(posInimigo);
          }
        }
      }

      // salva a ultima peca que foi movida
      m_PecaMovida = casaDaPosicao(m.posicaoInicial())->peca();

      // passa a peca para a posicao nova
      casaDaPosicao(m.posicaoFinal())->ocupar(casaDaPosicao(m.posicaoInicial())->peca());
      // atualiza a posicao da peca
      casaDaPosicao(m.posicaoInicial())->peca()->moverParaPosicao(m.posicaoFinal());
      // libera a casa da posicao inicial
      casaDaPosicao(m.posicaoInicial())->liberar();

      m_UltimoMov = m;
      m_MovimentosRealizados.push(m);
      m_HouveJogada = true;
    }
  }

}

/**
* Desfaz o ultimo movimento realizado
*/
void Tabuleiro::desmover() {
  if(houveJogada()) {
    Movimento m = m_MovimentosRealizados.top();
    m_MovimentosRealizados.pop();

    // se nao ha peca na posicao final do movimento, a jogada foi invalida
    if(casaDaPosicao(m.posicaoFinal())->estaLivre()) {
      throw 50;
    }

    std::shared_ptr<Peca> pecaMovida = casaDaPosicao(m.posicaoFinal())->peca();
    casaDaPosicao(m.posicaoInicial())->ocupar(pecaMovida);
    pecaMovida->moverParaPosicao(m.posicaoInicial());
    casaDaPosicao(m.posicaoFinal())->liberar();

    if(m.mataPeca()) {
      std::shared_ptr<Peca> pecaMorta = m_PecasMortas.top();
      m_PecasMortas.pop();
      casaDaPosicao(pecaMorta->posicao())->ocupar(pecaMorta);

      Cor corDaPeca = pecaMorta->cor();
      Tipo tipoDaPeca = pecaMorta->tipo();

      // volta os contadores de peca
      if(corDaPeca == BRANCA) {

        if(tipoDaPeca == PEAO) {
          m_NumPeoesBrancos += 1;
        } else if( tipoDaPeca == TORRE) {
          m_NumTorresBrancas += 1;
        }

      } else {

        if(tipoDaPeca == PEAO) {
          m_NumPeoesPretos += 1;
        } else if( tipoDaPeca == TORRE) {
          m_NumTorresPretas += 1;
        }

      }
    }

    if(m_MovimentosRealizados.empty()) {
      m_HouveJogada = false;
    }   
  }
}

Movimento Tabuleiro::ultimoMovimento() const {
  return m_UltimoMov;
}

bool Tabuleiro::houveJogada() const {
  return m_HouveJogada;
}

std::shared_ptr<Peca> Tabuleiro::pecaMovida() const {
  return m_PecaMovida;
}

bool Tabuleiro::posicaoValida(Posicao p) {
  bool linhaValida = p.linha() >= 0 && p.linha() < LINHAS;
  bool colunaValida = p.coluna() >= 0 && p.coluna() < COLUNAS;

  return linhaValida && colunaValida;
}

bool Tabuleiro::equal(Tabuleiro& o) {
  bool mesmoUltimoMovimento = m_UltimoMov==o.ultimoMovimento();
  bool mesmaHouveJogada = m_HouveJogada==o.houveJogada();
  bool mesmaCasa = true;
  for(int i=0; i<Tabuleiro::LINHAS&&mesmaCasa;i++){
    for(int j=0;j<Tabuleiro::COLUNAS&&mesmaCasa;j++){
      mesmaCasa = m_MapaDeCasas[i][j]->equal(*o.casaDaPosicao(Posicao(i,j)));
    }
  }
  return mesmoUltimoMovimento && mesmaHouveJogada && mesmaCasa;
}


/**
* Calcula o valor da funcao de avaliacao para o estado atual
*/
int Tabuleiro::valorHeuristica(Cor quemJoga) {
  int valor = 0;
  int invasaoBranca = 0;
  int invasaoPreta = 0;
  int distanciaInvasaoBranca = 0;
  int distanciaInvasaoPreta = 0;
  int meioDomPeoes =0;
  int torreInimigaLivre = 0;

  for(int i=0;i<Tabuleiro::LINHAS; i++){
    for(int j=0; j<Tabuleiro::COLUNAS; j++){
      Posicao p = Posicao(i,j);

      if(casaDaPosicao(p)->estaOcupada()){
        if(casaDaPosicao(p)->peca()->tipo() == PEAO){
          if(casaDaPosicao(p)->peca()->cor() == PRETA) {
            if(i==7) {
              // peao preto conseguiu invadir o territorio inimigo
              invasaoPreta = 1;
            }

            // distancia minima do peao preto para a base inimiga
            if(i>distanciaInvasaoPreta) {
              distanciaInvasaoPreta = i;
            }
          }

          if(casaDaPosicao(p)->peca()->cor() == BRANCA) {
            if(i==0) {
                  // peao branco conseguiu invadir o territorio inimigo
              invasaoBranca = 1;
            }

                // distancia minima ate invasao branca
            if(7-1 > distanciaInvasaoBranca) {
              distanciaInvasaoBranca = 7-i;
            }
          }

              // calcula dominio do meio do tabuleiro
          if(casaDaPosicao(p)->peca()->cor() == quemJoga){
            if(j>Tabuleiro::COLUNAS/2){
              meioDomPeoes+=(j-Tabuleiro::COLUNAS/2);
            }
            else{
              meioDomPeoes+=(Tabuleiro::COLUNAS/2 - j);
            }
          }
        } else {
            // liberdade da torre inimiga
          if(casaDaPosicao(p)->peca()->cor() != quemJoga){
                    //nao pode deixar uma torre inimiga livre
            bool verifica = false;
            for(int i=0;i<Tabuleiro::LINHAS; i++){
             Posicao p = Posicao(i,j);
             if(casaDaPosicao(p)->estaOcupada()){
              if(casaDaPosicao(p)->peca()->cor() == quemJoga){
                verifica = true;
                break;
              }
            }
          }
          if(verifica==false){
            torreInimigaLivre++;
          }
        }
      }
    }
  }
}

  // calcula valor de acordo com numero de pecas
  // Nao importa sacrificar alguns peoes desde que se mantenham as
  // torres ou matem as do inimigo.
  // criar para heuristica para nao ficar na mira de uma torre
if(quemJoga == BRANCA) {
  valor += 1000 * (invasaoBranca - invasaoPreta);
  valor += 500 * (distanciaInvasaoBranca - distanciaInvasaoPreta);
  valor += 80 * meioDomPeoes;
  valor += 200 * (numTorresBrancas() - numTorresPretas());
  valor += 110 * (numPeoesBrancos()  - numPeoesPretos());
  valor -= 1000 * torreInimigaLivre;
} else {
  valor += 1000 * (invasaoPreta - invasaoBranca);
  valor += 500 * (distanciaInvasaoPreta - distanciaInvasaoBranca);
  valor += 80 * meioDomPeoes;
  valor += 200 * (numTorresPretas() - numTorresBrancas());
  valor += 110 * (numPeoesPretos()  - numPeoesBrancos());
  valor -= 1000 * torreInimigaLivre;
}

return valor;
}

/**
* Gera todos os movimentos possiveis que a cor passada pode fazer
*/
vector<Movimento> Tabuleiro::gerarMovimentos(Cor quemJoga) {
  m_Movimentos.clear();
  for(int i=0; i<LINHAS; i++) {
    for(int j=0; j<COLUNAS; j++) {
      // checa se a posicao atual tem uma peca da cor que vai fazer a jogada
      Posicao pos = Posicao(i,j);
      if(casaDaPosicao(pos)->estaOcupada()) {
        if(pecaEmPosicao(pos)->cor() == quemJoga) {
          vector<Movimento>& movimentos = pecaEmPosicao(pos)->gerarMovimentos(this);
          m_Movimentos.insert( m_Movimentos.end(), movimentos.begin(), movimentos.end() );
        }
      }
    }
  }
  return m_Movimentos;
}

/**
* verifica se o tabuleiro atual eh um estado final do jogo.
*/
bool Tabuleiro::fimDeJogo(){
  int i;

  // checa se algum jogador chegou no outro lado
  for(i=0; i<COLUNAS; i++) {
    Posicao pBrancoVence = Posicao(0,i);
    Posicao pPretoVence = Posicao(7,i);
    
    if(!posicaoLivre(pBrancoVence)) {
      if(pecaEmPosicao(pBrancoVence)->tipo() == PEAO
        && pecaEmPosicao(pBrancoVence)->cor() == BRANCA) {
        m_CorVencedora = BRANCA;
      return true;
    }
  }

  if(!posicaoLivre(pPretoVence)) {
    if(pecaEmPosicao(pPretoVence)->tipo() == PEAO
      && pecaEmPosicao(pPretoVence)->cor() == PRETA) {
      m_CorVencedora = PRETA;
    return true;
  }
}
}

// checa se algum jogador conseguiu comer todas as pecas do outro
bool encontrouPreta  = (m_NumTorresPretas  + m_NumPeoesPretos)  > 0;
bool encontrouBranca = (m_NumTorresBrancas + m_NumPeoesBrancos) > 0;

if(!encontrouPreta) {
  m_CorVencedora = BRANCA;
  return true;
}

if(!encontrouBranca) {
  m_CorVencedora = PRETA;
  return true;
}

return false;
}
