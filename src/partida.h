#ifndef PARTIDA_H
#define PARTIDA_H

#include "tabuleiro.h"
#include "movimento.h"
#include "jogador.h"

class Partida {
  Tabuleiro &m_Tabuleiro;
  Jogador *m_JogadorPreto;
  Jogador *m_JogadorBranco;
  Jogador *m_JogadorAtual;
  Cor m_CorHumano;
  bool m_FoiIniciada;

public:
  Partida(Tabuleiro &t);
  void definirJogadorBranco(Jogador *j);
  void definirJogadorPreto(Jogador *j);
  void definirJogadorHumano(Cor cor);
  void trocarCores();
  void trocarVezJogador();
  bool vezDaMaquina();
  bool vezDoHumano();
  Movimento executaMaquina();
  Jogador *jogadorAtual();
  Jogador *jogadorHumano();
  Jogador *jogadorMaquina();
  void iniciar();
  bool foiIniciada();
  bool fimDeJogo();
  Jogador *vencedor();
};

#endif