#include "movimento.h"

Movimento::Movimento() {
  m_MataPeca = false;
}

/**
* Representa um movimento no tabuleiro. O atributo mataPeca
* confere se o movimento acarretara em morte de alguma peca.
*/
Movimento::Movimento(Posicao pi, Posicao pf, bool mataPeca) {
  m_PosicaoInicial = pi;
  m_PosicaoFinal = pf;
  m_MataPeca = mataPeca;
}

Posicao Movimento::posicaoInicial() const {
  return m_PosicaoInicial;
}

Posicao Movimento::posicaoFinal() const {
  return m_PosicaoFinal;
}

bool Movimento::mataPeca() const {
  return m_MataPeca;
}

bool Movimento::operator == (const Movimento &o) const {
  return o.posicaoInicial().equal(m_PosicaoInicial)
          && o.posicaoFinal().equal(m_PosicaoFinal)
          && m_MataPeca == o.mataPeca();
}

bool Movimento::operator!=(const Movimento &o) const {
  return !(*this == o);
}

std::ostream& operator<<(std::ostream& os, const Movimento& movimento) {
  os << "(" << movimento.posicaoInicial().linha()
            << "," << movimento.posicaoInicial().coluna()
            << ") -> (" << movimento.posicaoFinal().linha()
            << "," << movimento.posicaoFinal().coluna()
            << ") " << (movimento.mataPeca() ? "mata" : "nao mata");
  return os;
}