#include "../movimento.h"
#include "gtest/gtest.h"


// teste do construtor padrao do movimento
TEST(MovimentoTest, ConstrucaoPadrao) {
  Movimento movPadrao;

  EXPECT_TRUE(movPadrao.posicaoInicial().equal(Posicao(0,0)));
  EXPECT_TRUE(movPadrao.posicaoFinal().equal(Posicao(0,0)));
}

// testa a construcao do objeto movimento com valores
TEST(MovimentoTest, Construcao) {
  Movimento mov = Movimento(Posicao(1,2), Posicao(3,4), false);

  EXPECT_TRUE(mov.posicaoInicial().equal(Posicao(1,2)));
  EXPECT_TRUE(mov.posicaoFinal().equal(Posicao(3,4)));
}