#include "../casa.h"
#include "../peca.h"
#include "../torre.h"
#include "../peao.h"
#include "../posicao.h"
#include "gtest/gtest.h"
#include <memory>
#include <stdio.h>

TEST(CasaTest, Construcao) {
  Casa casaBranca = Casa(CASABRANCA);
  Casa casaPreta = Casa(CASAPRETA);

  ASSERT_TRUE(casaBranca.corDaCasa() == CASABRANCA);
  ASSERT_TRUE(casaPreta.corDaCasa() == CASAPRETA);
}

// testa o metodo equal
TEST(CasaTest, Equal) {
  std::shared_ptr<Peca> p1 = std::make_shared<Torre>(BRANCA, Posicao(0,0));
  std::shared_ptr<Peca> p2 = std::make_shared<Peao>(BRANCA, Posicao(1,1));

  Casa casaBrancaOcupadaP1 = Casa(CASABRANCA);
  Casa casaBrancaOcupadaP12 = Casa(CASABRANCA);
  Casa casaBrancaOcupadaP2 = Casa(CASABRANCA);
  Casa casaPretaOcupadaP1 = Casa(CASAPRETA);
  Casa casaPretaOcupadaP2 = Casa(CASAPRETA);
  Casa casaPreta = Casa(CASAPRETA);
  Casa casaBranca = Casa(CASABRANCA);

  casaBrancaOcupadaP1.ocupar(p1);
  casaBrancaOcupadaP12.ocupar(p1);
  casaBrancaOcupadaP2.ocupar(p2);
  casaPretaOcupadaP1.ocupar(p1);
  casaPretaOcupadaP2.ocupar(p2);

  ASSERT_TRUE(casaBrancaOcupadaP1.equal(casaBrancaOcupadaP1));
  ASSERT_TRUE(casaBrancaOcupadaP1.equal(casaBrancaOcupadaP12));
  ASSERT_TRUE(casaPretaOcupadaP1.equal(casaPretaOcupadaP1));
  ASSERT_TRUE(casaPreta.equal(casaPreta));
  ASSERT_FALSE(casaBrancaOcupadaP1.equal(casaBrancaOcupadaP2));
  ASSERT_FALSE(casaPretaOcupadaP1.equal(casaPretaOcupadaP2));
  ASSERT_FALSE(casaBranca.equal(casaPreta));
  ASSERT_FALSE(casaBranca.equal(casaBrancaOcupadaP1));
  ASSERT_FALSE(casaPreta.equal(casaPretaOcupadaP1));
  ASSERT_FALSE(casaPreta.equal(casaPretaOcupadaP1));
}

TEST(CasaTest, CasaLivre) {
  Casa casa = Casa(CASABRANCA);

  ASSERT_FALSE(casa.estaOcupada());
  ASSERT_TRUE(casa.estaLivre());
}

TEST(CasaTest, Ocupacao) {
  std::shared_ptr<Peca> p = std::make_shared<Torre>(BRANCA, Posicao(0,0));

  Casa casaOcupada = Casa(CASABRANCA);
  casaOcupada.ocupar(p);

  ASSERT_FALSE(casaOcupada.estaLivre());
  ASSERT_TRUE(casaOcupada.estaOcupada());
}

TEST(CasaTest, Liberacao) {
  std::shared_ptr<Peca> p =  std::make_shared<Torre>(BRANCA, Posicao(0,0));

  Casa casa = Casa(CASABRANCA);
  casa.ocupar(p);
  casa.liberar();

  ASSERT_FALSE(casa.estaOcupada());
  ASSERT_TRUE(casa.estaLivre());
}

TEST(CasaTest, ConstrutorCopy){
  Casa casaOriginal = Casa(CASABRANCA);
  std::shared_ptr<Peca> p = std::make_shared<Torre>(BRANCA, Posicao(0,0));
  casaOriginal.ocupar(p);

  Casa casaCopiada = Casa(casaOriginal);

  std::shared_ptr<Peca> pecaOriginal = casaOriginal.peca();
  std::shared_ptr<Peca> pecaCopiada = casaCopiada.peca();

  pecaCopiada->moverParaPosicao(Posicao(1,1));
  Posicao posicaoOriginal = pecaOriginal->posicao();
  Posicao posicaoCopiada = pecaCopiada->posicao();
  (pecaOriginal.get())->posicao();
  
  ASSERT_NE(pecaOriginal.get(), pecaCopiada.get());
  ASSERT_FALSE(posicaoOriginal.equal(posicaoCopiada));
}
