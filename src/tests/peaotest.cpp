#include "../peca.h"
#include "../peao.h"
#include "../torre.h"
#include "../posicao.h"
#include "../movimento.h"
#include "../tabuleiro.h"
#include "gtest/gtest.h"
#include <string>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <memory>

// testa a criacao de peoes
TEST(PeaoTest, Construcao) {
  Posicao p = Posicao(0,0);
  Peao peaoBranco = Peao(BRANCA, p);
  Peao peaoPreto = Peao(PRETA, p);

  ASSERT_EQ(BRANCA, peaoBranco.cor());
  ASSERT_EQ(PRETA, peaoPreto.cor());
  ASSERT_STRCASEEQ("peão", peaoBranco.nome().c_str());
  ASSERT_STRCASEEQ("peão", peaoPreto.nome().c_str());
  ASSERT_TRUE(peaoBranco.posicao().equal(p));
  ASSERT_TRUE(peaoPreto.posicao().equal(p));
}

// testa o metodo equal
TEST(PeaoTest, Equal) {
  Posicao p = Posicao(0,0);
  Peao peaoBranco = Peao(BRANCA, p);
  Peao peaoPreto = Peao(PRETA, p);
  Torre torre = Torre(BRANCA, p);

  ASSERT_TRUE(peaoBranco.equal(peaoBranco));
  ASSERT_TRUE(peaoPreto.equal(peaoPreto));
  ASSERT_FALSE(peaoBranco.equal(peaoPreto));
  ASSERT_FALSE(peaoPreto.equal(peaoBranco));
  ASSERT_FALSE(peaoBranco.equal(torre));
  ASSERT_FALSE(peaoPreto.equal(torre));
  ASSERT_FALSE(torre.equal(peaoBranco));
  ASSERT_FALSE(torre.equal(peaoPreto));
}

// testa o alteracao de posicao de pecoes
TEST(PeaoTest, MoverPeao) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Peao peao = Peao(BRANCA, pi);
  peao.moverParaPosicao(pf);
  ASSERT_TRUE(peao.posicao().equal(pf));
}

// testa movimentos basicos validos
TEST(PeaoTest, MovimentosValidos) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  ASSERT_TRUE(peao->movimentoValido(Posicao(pos.linha() - 2, pos.coluna()), t));
  ASSERT_TRUE(peao->movimentoValido(Posicao(pos.linha() - 1, pos.coluna()), t));

  delete t;
}

// testa movimentos de comer peoes nas diagonais
TEST(PeaoTest, MovimentosComerPeoes) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna() - 1);
  Posicao posP2 = Posicao(pos.linha()-1, pos.coluna() + 1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Peao> peaoPreto1 = std::make_shared<Peao>(PRETA, posP1);
  std::shared_ptr<Peao> peaoPreto2 = std::make_shared<Peao>(PRETA, posP2);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posP1)->ocupar(peaoPreto1);
  t->casaDaPosicao(posP2)->ocupar(peaoPreto2);

  ASSERT_TRUE(peao->movimentoValido(posP1, t));
  ASSERT_TRUE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimento en passant
TEST(PeaoTest, EnPassant) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, 0);
  Posicao posPassant = Posicao(pos.linha() - 2, pos.coluna());
  Posicao posOponente = Posicao(pos.linha()-2, 1);
  Posicao posFinal = Posicao(pos.linha()-1, pos.coluna());

  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Peao> peaoPreto = std::make_shared<Peao>(PRETA, posOponente);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posOponente)->ocupar(peaoPreto);
  t->mover(Movimento(pos, posPassant, false));

  ASSERT_TRUE(peaoPreto->movimentoValido(posFinal, t));

  t->mover(Movimento(posOponente, posFinal, true));

  ASSERT_TRUE(peaoPreto->posicao().equal(posFinal));
  ASSERT_TRUE(t->casaDaPosicao(posPassant)->estaLivre());

  delete t;
}

// testa movimentos de comer tores nas diagonais
TEST(PeaoTest, MovimentosComerTorres) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna() - 1);
  Posicao posP2 = Posicao(pos.linha()-1, pos.coluna() + 1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Torre> torrePreta1 = std::make_shared<Torre>(PRETA, posP1);
  std::shared_ptr<Torre> torrePreta2 = std::make_shared<Torre>(PRETA, posP2);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posP1)->ocupar(torrePreta1);
  t->casaDaPosicao(posP2)->ocupar(torrePreta2);

  ASSERT_TRUE(peao->movimentoValido(posP1, t));
  ASSERT_TRUE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de comer pecas invalidas (mesma cor)
TEST(PeaoTest, MovimentosComerPecaInvalida) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna() - 1);
  Posicao posP2 = Posicao(pos.linha()-1, pos.coluna() + 1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Peao> peao1 = std::make_shared<Peao>(BRANCA, posP1);
  std::shared_ptr<Peao> peao2 = std::make_shared<Peao>(BRANCA, posP2);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posP1)->ocupar(peao1);
  t->casaDaPosicao(posP2)->ocupar(peao2);

  ASSERT_FALSE(peao->movimentoValido(posP1, t));
  ASSERT_FALSE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de comer pecas invalidas (sem pecas)
TEST(PeaoTest, MovimentosComerPecaInexistentes) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna() - 1);
  Posicao posP2 = Posicao(pos.linha()-1, pos.coluna() + 1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  ASSERT_FALSE(peao->movimentoValido(posP1, t));
  ASSERT_FALSE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de avanco de casas
TEST(PeaoTest, MovimentosAvancar) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha()-2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  ASSERT_TRUE(peao->movimentoValido(posP1, t));
  ASSERT_TRUE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de avanco de casas quando ja moveu
TEST(PeaoTest, MovimentosAvancarDepoisDeJogada) {
  Posicao posOriginal = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao pos = Posicao(posOriginal.linha() - 1, posOriginal.coluna());
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha()-2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, posOriginal);
  // so para fingir que ja houve uma jogada => impedir pular 2 casas
  peao->moverParaPosicao(pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  ASSERT_TRUE(peao->movimentoValido(posP1, t));
  ASSERT_FALSE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de avanco de casas bloqueados
TEST(PeaoTest, MovimentosAvancarBoqueados) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha()-2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Peao> peaoPreto1 = std::make_shared<Peao>(PRETA, posP1);
  std::shared_ptr<Peao> peaoPreto2 = std::make_shared<Peao>(PRETA, posP2);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posP1)->ocupar(peaoPreto1);
  t->casaDaPosicao(posP2)->ocupar(peaoPreto2);

  ASSERT_FALSE(peao->movimentoValido(posP1, t));
  ASSERT_FALSE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de avanco de casa intermediaria bloqueada
TEST(PeaoTest, MovimentosAvancarPosIntermediariaBloqueada) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha()-2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Peao> peaoPreto1 = std::make_shared<Peao>(PRETA, posP1);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posP1)->ocupar(peaoPreto1);

  ASSERT_FALSE(peao->movimentoValido(posP1, t));
  ASSERT_FALSE(peao->movimentoValido(posP2, t));

  delete t;
}

// testa movimentos de avanco de casa avanca duas bloqueada
TEST(PeaoTest, MovimentosAvancarPosAvancadaBloqueada) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha()-1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha()-2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  std::shared_ptr<Peao> peaoPreto1 = std::make_shared<Peao>(PRETA, posP2);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posP2)->ocupar(peaoPreto1);

  ASSERT_TRUE(peao->movimentoValido(posP1, t));
  ASSERT_FALSE(peao->movimentoValido(posP2, t));

  delete t;
}

// peca branca na ultima linha => nenhum movimento possivel
TEST(PeaoTest, MovimentosBrancaNoLimite) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  for(int i=0; i<Tabuleiro::LINHAS; i++) {
    for(int j=0; j<Tabuleiro::COLUNAS; j++) {
      Posicao p = Posicao(i,j);
      ASSERT_FALSE(peao->movimentoValido(p, t));
    }
  }

  delete t;
}

// peca preta na ultima linha => nenhum movimento possivel
TEST(PeaoTest, MovimentosPretaNoLimite) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  for(int i=0; i<Tabuleiro::LINHAS; i++) {
    for(int j=0; j<Tabuleiro::COLUNAS; j++) {
      Posicao p = Posicao(i,j);
      ASSERT_FALSE(peao->movimentoValido(p, t));
    }
  }

  delete t;
}

// testa o gerador de movimentos para a posicao inicial dos peoes brancos
TEST(PeaoTest, GerarMovimentosIniciaisBrancos) {
  Posicao pos = Posicao(Tabuleiro::LINHAS - 1, (int)floor(Tabuleiro::COLUNAS / 2));
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(2, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()-1, pos.coluna()), false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()-2, pos.coluna()), false)));

  delete t;
}

// testa o gerador de movimentos para a posicao inicial dos peoes pretos
TEST(PeaoTest, GerarMovimentosIniciaisPretos) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(2, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()),false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+2, pos.coluna()),false)));

  delete t;
}

// testa o gerador de movimentos para a posicao inicial dos peoes pretos com opcao de comer
TEST(PeaoTest, GerarMovimentosIniciaisComUmaVitima) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posVitima = Posicao(pos.linha() + 1, pos.coluna() +1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoVitima = std::make_shared<Peao>(BRANCA, posVitima);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posVitima)->ocupar(peaoVitima);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(3, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()), false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+2, pos.coluna()), false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posVitima, true)));

  delete t;
}

// testa o gerador de movimentos para a posicao inicial dos peoes pretos com opcao de comer falsa
// (peao da mesma cor)
TEST(PeaoTest, GerarMovimentosIniciaisComUmaVitimaFalsa) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posVitima = Posicao(pos.linha() + 1, pos.coluna() +1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoVitima = std::make_shared<Peao>(PRETA, posVitima);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posVitima)->ocupar(peaoVitima);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(2, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()), false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+2, pos.coluna()), false)));

  delete t;
}

// testa o gerador de movimentos para a posicao inicial dos peoes pretos com 2 pcoes de comer
TEST(PeaoTest, GerarMovimentosIniciaisComDuasVitimas) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posVitima1 = Posicao(pos.linha() + 1, pos.coluna() +1);
  Posicao posVitima2 = Posicao(pos.linha() + 1, pos.coluna() -1);
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoVitima1 = std::make_shared<Peao>(BRANCA, posVitima1);
  std::shared_ptr<Peao> peaoVitima2 = std::make_shared<Peao>(BRANCA, posVitima2);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posVitima1)->ocupar(peaoVitima1);
  t->casaDaPosicao(posVitima2)->ocupar(peaoVitima2);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(4, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()),false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+2, pos.coluna()),false)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posVitima1, true)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posVitima2, true)));

  delete t;
}

// testa o gerador de movimentos para peao bloqueado por oponente
TEST(PeaoTest, GerarMovimentosIniciaisPeaoBloqueadoPorOponente) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posBloqueador = Posicao(pos.linha() + 1, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoBloqueador = std::make_shared<Peao>(BRANCA, posBloqueador);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posBloqueador)->ocupar(peaoBloqueador);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(0, (int)m.size());

  delete t;
}

// testa o gerador de movimentos para peao bloqueado por amigo
TEST(PeaoTest, GerarMovimentosIniciaisPeaoBloqueadoPorAmigo) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posBloqueador = Posicao(pos.linha() + 1, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoBloqueador = std::make_shared<Peao>(PRETA, posBloqueador);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posBloqueador)->ocupar(peaoBloqueador);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(0, (int)m.size());

  delete t;
}

// testa o gerador de movimentos para peao bloqueado por oponente duas a frente
TEST(PeaoTest, GerarMovimentosIniciaisPeaoBloqueadoOponenteAvancadoDuas) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posBloqueador = Posicao(pos.linha() + 2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoBloqueador = std::make_shared<Peao>(BRANCA, posBloqueador);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posBloqueador)->ocupar(peaoBloqueador);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(1, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()), false)));

  delete t;
}

// testa o gerador de movimentos para peao bloqueado por amigo duas a frente
TEST(PeaoTest, GerarMovimentosIniciaisPeaoBloqueadoAmigoAvancadoDuas) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posBloqueador = Posicao(pos.linha() + 2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoBloqueador = std::make_shared<Peao>(PRETA, posBloqueador);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posBloqueador)->ocupar(peaoBloqueador);

  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(1, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()), false)));

  delete t;
}


// testa o gerador de movimentos com chamadas consecutivas (vetor deve ser reiniciado)
TEST(PeaoTest, GerarMovimentosChamadasConsecutivas) {
  Posicao pos = Posicao(0, (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posBloqueador = Posicao(pos.linha() + 2, pos.coluna());
  std::shared_ptr<Peao> peao = std::make_shared<Peao>(PRETA, pos);
  std::shared_ptr<Peao> peaoBloqueador = std::make_shared<Peao>(PRETA, posBloqueador);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(peao);
  t->casaDaPosicao(posBloqueador)->ocupar(peaoBloqueador);

  peao->gerarMovimentos(t);
  peao->gerarMovimentos(t);
  vector<Movimento>& m = peao->gerarMovimentos(t);

  ASSERT_EQ(1, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha()+1, pos.coluna()), false)));

  delete t;
}