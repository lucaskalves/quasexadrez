#include "../tabuleiro.h"
#include "../posicao.h"
#include "../peca.h"
#include "../peao.h"
#include "../movimento.h"
#include "../casa.h"
#include "gtest/gtest.h"
#include <memory>


// testa se o construtor aloca as casas corretamente
TEST(TabuleiroTest, ConstrucaoTabuleiro) {
  Tabuleiro t = Tabuleiro();

  for(int i=0; i<Tabuleiro::LINHAS; i++) {
    for(int j=0; j<Tabuleiro::COLUNAS; j++) {
      EXPECT_TRUE(t.posicaoLivre(Posicao(i,j)));
    }
  }

  EXPECT_FALSE(t.houveJogada());
}

// testa o metodo que monta o tabuleiro inicial
TEST(TabuleiroTest, MontarTabuleiro) {
  Tabuleiro t = Tabuleiro();
  t.montarTabuleiroInicial();

  for(int i=0; i<Tabuleiro::LINHAS; i++) {
    for(int j=0; j<Tabuleiro::COLUNAS; j++) {
      if(i == 0 && j == 0) {
        // torre preta da esquerda
        EXPECT_FALSE(t.posicaoLivre(Posicao(i,j)));
      } else if (i == 0 && j == 7) {
        // torre preta da direit
        EXPECT_FALSE(t.posicaoLivre(Posicao(i,j)));
      } else if (i == 7 && j == 0) {
        // torre branca da esquerda
        EXPECT_FALSE(t.posicaoLivre(Posicao(i,j)));
      } else if (i == 7 && j == 7) {
        // torre branca da direit
        EXPECT_FALSE(t.posicaoLivre(Posicao(i,j)));
      } else if (i == 1) {
        // peoes pretos
        EXPECT_FALSE(t.posicaoLivre(Posicao(i,j)));
      } else if (i == 6) {
        // peoes brancos
        EXPECT_FALSE(t.posicaoLivre(Posicao(i,j)));
      } else {
        EXPECT_TRUE(t.posicaoLivre(Posicao(i,j)));
      }
    }
  }

  EXPECT_FALSE(t.houveJogada());
}

// testa o movimento de uma peca para uma casa vazia
TEST(TabuleiroTest, MoverPeca) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pi);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p);

  EXPECT_FALSE(t.posicaoLivre(pi));

  t.mover(m);
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));
}

// testa desfazer movimento
TEST(TabuleiroTest, DesmoverPeca) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pi);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p);

  EXPECT_FALSE(t.posicaoLivre(pi));

  t.mover(m);
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.desmover();
  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));
}

// testa desfazer movimentos
TEST(TabuleiroTest, DesmoverPecaDuasVezes) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  Movimento mVolta = Movimento(pf, pi, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pi);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p);

  EXPECT_FALSE(t.posicaoLivre(pi));

  t.mover(m);
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.mover(mVolta);
  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));

  t.desmover();
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.desmover();
  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));
}

// testa desfazer movimentos e fazer denovo e desmover
TEST(TabuleiroTest, DesmoverMoverDesmover) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pi);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p);

  t.mover(m);
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.desmover();
  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));

  t.mover(m);
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.desmover();
  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));
}

// testa desmover quando come peca
TEST(TabuleiroTest, DesmoverComendo) {
  Tabuleiro t = Tabuleiro();
  t.montarTabuleiroInicial();
  Movimento m = Movimento(Posicao(0,0),Posicao(1,0), true);

  // estado inicial
  EXPECT_EQ(8, t.numPeoesPretos());
  EXPECT_FALSE(t.posicaoLivre(m.posicaoInicial()));
  EXPECT_FALSE(t.posicaoLivre(m.posicaoFinal()));

  // movimento: torre preta do canto esquerdo come peao preta da sua frente
  t.mover(m);
  EXPECT_EQ(7, t.numPeoesPretos());
  EXPECT_TRUE(t.posicaoLivre(m.posicaoInicial()));
  EXPECT_FALSE(t.posicaoLivre(m.posicaoFinal()));

  t.desmover();
  EXPECT_EQ(8, t.numPeoesPretos());
  EXPECT_FALSE(t.posicaoLivre(m.posicaoInicial()));
  EXPECT_FALSE(t.posicaoLivre(m.posicaoFinal()));
}


// testa o calculo do numero de pecas
TEST(TabuleiroTest, ContadoresDePecas) {
  Tabuleiro t = Tabuleiro();
  t.montarTabuleiroInicial();

  EXPECT_EQ(8, t.numPeoesBrancos());
  EXPECT_EQ(8, t.numPeoesPretos());
  EXPECT_EQ(2, t.numTorresBrancas());
  EXPECT_EQ(2, t.numTorresPretas());

  // faz um movimento de comer peca qualquer
  // esse movimento eh invalido, mas como a funcao mover nao avalia validade,
  // o movimento serve para testar o contador
  // movimento: torre preta do canto esquerdo come peao preta da sua frente
  t.mover(Movimento(Posicao(0,0),Posicao(1,0), true));
  EXPECT_EQ(7, t.numPeoesPretos());

  // movimento: peao preto do canto esquerdo come torre preta de tras
  t.mover(Movimento(Posicao(1,7),Posicao(0,7), true));
  EXPECT_EQ(1, t.numTorresPretas());

  // movimento: torre branco do canto esquerdo come peao branco da frente
  t.mover(Movimento(Posicao(7,0),Posicao(6,0), true));
  EXPECT_EQ(7, t.numPeoesBrancos());

  // movimento: peao branco do canto esquerdo come torre branca de tras
  t.mover(Movimento(Posicao(6,7),Posicao(7,7), true));
  EXPECT_EQ(1, t.numTorresBrancas());
}

// testa o movimento de uma peca para uma casa ocupada
TEST(TabuleiroTest, MoverPecaComendo) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, true);
  std::shared_ptr<Peca> p1 = std::make_shared<Peao>(BRANCA, pi);
  std::shared_ptr<Peca> p2 = std::make_shared<Peao>(PRETA, pf);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p1);
  t.casaDaPosicao(pf)->ocupar(p2);

  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.mover(m);
  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));
}

// testa um movimento entre casas vazias (nada deve ser alterado)
TEST(TabuleiroTest, MoverSemPecas) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  Tabuleiro t = Tabuleiro();

  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));

  t.mover(m);

  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_TRUE(t.posicaoLivre(pf));
}

// testa o movimento de uma casa vazia para uma ocupada
TEST(TabuleiroTest, MoverSemPecaDePartida) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pf);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pf)->ocupar(p);

  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.mover(m);

  EXPECT_TRUE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));
}

// testa o movimento de uma casa para ela mesma (nada deve mudar)
TEST(TabuleiroTest, MoverParaPropriaCasa) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(0,0);
  Movimento m = Movimento(pi, pf, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pi);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p);

  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));

  t.mover(m);

  EXPECT_FALSE(t.posicaoLivre(pi));
  EXPECT_FALSE(t.posicaoLivre(pf));
}

// testa o metodo para pegar movimento anterior
TEST(TabuleiroTest, PegarUltimoMovimento) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Movimento m = Movimento(pi, pf, false);
  std::shared_ptr<Peca> p = std::make_shared<Peao>(BRANCA, pi);

  Tabuleiro t = Tabuleiro();
  t.casaDaPosicao(pi)->ocupar(p);
  t.mover(m);
  EXPECT_TRUE(t.houveJogada());
  EXPECT_TRUE(t.ultimoMovimento().posicaoInicial().equal(pi));
  EXPECT_TRUE(t.ultimoMovimento().posicaoFinal().equal(pf));
}

//testa se o construtor copy esta bem implementado
TEST(TabuleiroTest, ConstrutorCopy) {
    int i,j;
    Tabuleiro t1 = Tabuleiro();
    t1.montarTabuleiroInicial();
    Tabuleiro t2 = Tabuleiro(t1);

    EXPECT_EQ(t1.numTorresBrancas(), t2.numTorresBrancas());
    EXPECT_EQ(t1.numTorresPretas(), t2.numTorresPretas());
    EXPECT_EQ(t1.numPeoesBrancos(), t2.numPeoesBrancos());
    EXPECT_EQ(t1.numPeoesPretos(), t2.numPeoesPretos());

    for(i=0;i<Tabuleiro::LINHAS;i++){
        for(j=0;j<Tabuleiro::COLUNAS;j++){
            EXPECT_NE(t1.casaDaPosicao(Posicao(i,j)).get(),t2.casaDaPosicao(Posicao(i,j)).get());
            EXPECT_EQ(t1.casaDaPosicao(Posicao(i,j))->estaOcupada(), t2.casaDaPosicao(Posicao(i,j))->estaOcupada());
            EXPECT_EQ(t1.casaDaPosicao(Posicao(i,j))->corDaCasa(), t2.casaDaPosicao(Posicao(i,j))->corDaCasa());
            if(t1.casaDaPosicao(Posicao(i,j))->estaOcupada()) {
              EXPECT_NE(t1.casaDaPosicao(Posicao(i,j))->peca().get(),t2.casaDaPosicao(Posicao(i,j))->peca().get());
              EXPECT_EQ(t1.casaDaPosicao(Posicao(i,j))->peca()->cor(),t2.casaDaPosicao(Posicao(i,j))->peca()->cor());
              EXPECT_EQ(t1.casaDaPosicao(Posicao(i,j))->peca()->tipo(),t2.casaDaPosicao(Posicao(i,j))->peca()->tipo());
              EXPECT_TRUE(t1.casaDaPosicao(Posicao(i,j))->peca()->posicao().equal(t2.casaDaPosicao(Posicao(i,j))->peca()->posicao()));
            }
        }
    }
}

TEST(TabuleiroTest, EqualTest){
    Tabuleiro t1 = Tabuleiro();
    t1.montarTabuleiroInicial();
    Tabuleiro t2 = Tabuleiro(t1);
    EXPECT_TRUE(t1.equal(t2));
    t2.mover(Movimento(Posicao(1,1), Posicao(1,2), false));
    EXPECT_FALSE(t1.equal(t2));
}
