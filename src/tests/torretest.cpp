#include "../peca.h"
#include "../torre.h"
#include "../peao.h"
#include "../posicao.h"
#include "../movimento.h"
#include "../tabuleiro.h"
#include "gtest/gtest.h"
#include <string>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <memory>

// testa a criacao de torres
TEST(TorreTest, Construcao) {
  Posicao p = Posicao(0,0);
  Torre torreBranca = Torre(BRANCA, p);
  Torre torrePreta = Torre(PRETA, p);

  ASSERT_EQ(BRANCA, torreBranca.cor());
  ASSERT_EQ(PRETA, torrePreta.cor());
  ASSERT_STRCASEEQ("torre", torreBranca.nome().c_str());
  ASSERT_STRCASEEQ("torre", torrePreta.nome().c_str());
  ASSERT_TRUE(torreBranca.posicao().equal(p));
  ASSERT_TRUE(torrePreta.posicao().equal(p));
}


// testa o metodo equal
TEST(TorreTest, Equal) {
  Posicao p = Posicao(0,0);
  Torre torreBranca = Torre(BRANCA, p);
  Torre torrePreta = Torre(PRETA, p);
  Peao peao = Peao(BRANCA, p);

  ASSERT_TRUE(torreBranca.equal(torreBranca));
  ASSERT_TRUE(torrePreta.equal(torrePreta));
  ASSERT_FALSE(torreBranca.equal(torrePreta));
  ASSERT_FALSE(torrePreta.equal(torreBranca));
  ASSERT_FALSE(torreBranca.equal(peao));
  ASSERT_FALSE(torrePreta.equal(peao));
  ASSERT_FALSE(peao.equal(torreBranca));
  ASSERT_FALSE(peao.equal(torrePreta));
}


// testa o alteracao de posicao de torres
TEST(TorreTest, MoverTorre) {
  Posicao pi = Posicao(0,0);
  Posicao pf = Posicao(2,0);
  Torre torre = Torre(BRANCA, pi);
  torre.moverParaPosicao(pf);
  ASSERT_TRUE(torre.posicao().equal(pf));
}

// testa movimentos basicos validos com a torre livre
TEST(TorreTest, MovimentosValidos) {
  // coloca a torre no meio do tabuleiro, mais ou menos
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);

  // testa movimentos verticais
  for(int i=0; i<Tabuleiro::LINHAS; i++) {
    if(i != pos.linha()) {
      ASSERT_TRUE(torre->movimentoValido(Posicao(i, pos.coluna()), t));
    } else {
      // movimento para a propria casa eh invalido
      ASSERT_FALSE(torre->movimentoValido(Posicao(i, pos.coluna()), t));
    }
  }

  // testa movimentos horizontais
  for(int j=0; j<Tabuleiro::COLUNAS; j++) {
    if(j != pos.coluna()) {
      ASSERT_TRUE(torre->movimentoValido(Posicao(pos.linha(), j), t));
    } else {
      // movimento para a propria casa eh invalido
      ASSERT_FALSE(torre->movimentoValido(Posicao(pos.linha(), j), t));
    }
  }

  delete t;
}

// testa movimentos de tore branca comer torres pretas ao redor de torre branca
TEST(TorreTest, MovimentosComerTorres) {
  // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torrePreta1 = std::make_shared<Torre>(PRETA, posP1);
  std::shared_ptr<Torre> torrePreta2 = std::make_shared<Torre>(PRETA, posP2);
  std::shared_ptr<Torre> torrePreta3 = std::make_shared<Torre>(PRETA, posP3);
  std::shared_ptr<Torre> torrePreta4 = std::make_shared<Torre>(PRETA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torrePreta1);
  t->casaDaPosicao(posP2)->ocupar(torrePreta2);
  t->casaDaPosicao(posP3)->ocupar(torrePreta3);
  t->casaDaPosicao(posP4)->ocupar(torrePreta4);

  ASSERT_TRUE(torre->movimentoValido(posP1, t));
  ASSERT_TRUE(torre->movimentoValido(posP2, t));
  ASSERT_TRUE(torre->movimentoValido(posP3, t));
  ASSERT_TRUE(torre->movimentoValido(posP4, t));

  delete t;
}

// testa movimentos de torre branca comer peoes pretos ao redor de torre branca
TEST(TorreTest, MovimentosComerPeoes) {
  // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Peao> peaoPreto1 = std::make_shared<Peao>(PRETA, posP1);
  std::shared_ptr<Peao> peaoPreto2 = std::make_shared<Peao>(PRETA, posP2);
  std::shared_ptr<Peao> peaoPreto3 = std::make_shared<Peao>(PRETA, posP3);
  std::shared_ptr<Peao> peaoPreto4 = std::make_shared<Peao>(PRETA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(peaoPreto1);
  t->casaDaPosicao(posP2)->ocupar(peaoPreto2);
  t->casaDaPosicao(posP3)->ocupar(peaoPreto3);
  t->casaDaPosicao(posP4)->ocupar(peaoPreto4);

  ASSERT_TRUE(torre->movimentoValido(posP1, t));
  ASSERT_TRUE(torre->movimentoValido(posP2, t));
  ASSERT_TRUE(torre->movimentoValido(posP3, t));
  ASSERT_TRUE(torre->movimentoValido(posP4, t));

  delete t;
}

// testa movimentos de comer pecas invalidas (mesma cor)
TEST(TorreTest, MovimentosComerPecaInvalida) {
  // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torreBranca1 = std::make_shared<Torre>(BRANCA, posP1);
  std::shared_ptr<Torre> torreBranca2 = std::make_shared<Torre>(BRANCA, posP2);
  std::shared_ptr<Torre> torreBranca3 = std::make_shared<Torre>(BRANCA, posP3);
  std::shared_ptr<Torre> torreBranca4 = std::make_shared<Torre>(BRANCA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torreBranca1);
  t->casaDaPosicao(posP2)->ocupar(torreBranca2);
  t->casaDaPosicao(posP3)->ocupar(torreBranca3);
  t->casaDaPosicao(posP4)->ocupar(torreBranca4);

  ASSERT_FALSE(torre->movimentoValido(posP1, t));
  ASSERT_FALSE(torre->movimentoValido(posP2, t));
  ASSERT_FALSE(torre->movimentoValido(posP3, t));
  ASSERT_FALSE(torre->movimentoValido(posP4, t));

  delete t;
}

// testa movimentos de avanco de casa intermediaria bloqueada
TEST(TorreTest, MovimentosAvancarPosIntermediariaBloqueadaPorAmigo) {
  // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torreBranca1 = std::make_shared<Torre>(BRANCA, posP1);
  std::shared_ptr<Torre> torreBranca2 = std::make_shared<Torre>(BRANCA, posP2);
  std::shared_ptr<Torre> torreBranca3 = std::make_shared<Torre>(BRANCA, posP3);
  std::shared_ptr<Torre> torreBranca4 = std::make_shared<Torre>(BRANCA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torreBranca1);
  t->casaDaPosicao(posP2)->ocupar(torreBranca2);
  t->casaDaPosicao(posP3)->ocupar(torreBranca3);
  t->casaDaPosicao(posP4)->ocupar(torreBranca4);

  // tenta atravessar a posicao que esta ocupada
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP1.linha()-1, posP1.coluna()), t));
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP2.linha()+1, posP2.coluna()), t));
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP3.linha(), posP3.coluna()-1), t));
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP4.linha(), posP4.coluna()+1), t));

  delete t;
}

// testa movimentos de avanco de casa intermediaria bloqueada
TEST(TorreTest, MovimentosAvancarPosIntermediariaBloqueadaPorOponente) {
  // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torrePreta1 = std::make_shared<Torre>(PRETA, posP1);
  std::shared_ptr<Torre> torrePreta2 = std::make_shared<Torre>(PRETA, posP2);
  std::shared_ptr<Torre> torrePreta3 = std::make_shared<Torre>(PRETA, posP3);
  std::shared_ptr<Torre> torrePreta4 = std::make_shared<Torre>(PRETA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torrePreta1);
  t->casaDaPosicao(posP2)->ocupar(torrePreta2);
  t->casaDaPosicao(posP3)->ocupar(torrePreta3);
  t->casaDaPosicao(posP4)->ocupar(torrePreta4);

  // tenta atravessar a posicao que esta ocupada
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP1.linha()-1, posP1.coluna()), t));
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP2.linha()+1, posP2.coluna()), t));
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP3.linha(), posP3.coluna()-1), t));
  ASSERT_FALSE(torre->movimentoValido(Posicao(posP4.linha(), posP4.coluna()+1), t));

  delete t;
}

// testa o gerador de movimentos para a posicao inicial das torres brancos
TEST(TorreTest, GerarMovimentosIniciaisBrancas) {
    // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);

  vector<Movimento>& m = torre->gerarMovimentos(t);

  ASSERT_EQ(Tabuleiro::LINHAS + Tabuleiro::COLUNAS - 2, (int)m.size());

  for(int i = 0; i<Tabuleiro::LINHAS; i++) {
    if(i != pos.linha()) {
      ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(i, pos.coluna()), false)));
    }
  }

  for(int j = 0; j<Tabuleiro::LINHAS; j++) {
    if(j != pos.coluna()) {
      ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(pos.linha(), j), false)));
    }
  }

  delete t;
}

// testa o gerador de movimentos para a posicao inicial de torre com 4 vitimas ao redor
TEST(TorreTest, GerarMovimentosIniciaisComUmaVitima) {
  // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torrePreta1 = std::make_shared<Torre>(PRETA, posP1);
  std::shared_ptr<Torre> torrePreta2 = std::make_shared<Torre>(PRETA, posP2);
  std::shared_ptr<Torre> torrePreta3 = std::make_shared<Torre>(PRETA, posP3);
  std::shared_ptr<Torre> torrePreta4 = std::make_shared<Torre>(PRETA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torrePreta1);
  t->casaDaPosicao(posP2)->ocupar(torrePreta2);
  t->casaDaPosicao(posP3)->ocupar(torrePreta3);
  t->casaDaPosicao(posP4)->ocupar(torrePreta4);

  vector<Movimento>& m = torre->gerarMovimentos(t);

  ASSERT_EQ(4, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posP1, true)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posP2, true)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posP3, true)));
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, posP4, true)));

  delete t;
}

// testa o gerador de movimentos para a torre bloqueada por 4 amigos
TEST(TorreTest, GerarMovimentosTorreBloqueada) {
 // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 1);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torreBranca1 = std::make_shared<Torre>(BRANCA, posP1);
  std::shared_ptr<Torre> torreBranca2 = std::make_shared<Torre>(BRANCA, posP2);
  std::shared_ptr<Torre> torreBranca3 = std::make_shared<Torre>(BRANCA, posP3);
  std::shared_ptr<Torre> torreBranca4 = std::make_shared<Torre>(BRANCA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torreBranca1);
  t->casaDaPosicao(posP2)->ocupar(torreBranca2);
  t->casaDaPosicao(posP3)->ocupar(torreBranca3);
  t->casaDaPosicao(posP4)->ocupar(torreBranca4);

  vector<Movimento>& m = torre->gerarMovimentos(t);

  ASSERT_EQ(0, (int)m.size());

  delete t;
}

// testa o gerador de movimentos com chamadas consecutivas (vetor deve ser reiniciado)
TEST(TorreTest, GerarMovimentosChamadasConsecutivas) {
 // coloca a torre no meio do tabuleiro
  Posicao pos = Posicao((int)floor(Tabuleiro::LINHAS / 2), (int)floor(Tabuleiro::COLUNAS / 2));
  Posicao posP1 = Posicao(pos.linha() - 1, pos.coluna());
  Posicao posP2 = Posicao(pos.linha() + 1, pos.coluna());
  Posicao posP3 = Posicao(pos.linha(), pos.coluna() - 1);
  //unico movimento livre: coluna + 1
  Posicao posP4 = Posicao(pos.linha(), pos.coluna() + 2);

  std::shared_ptr<Torre> torre = std::make_shared<Torre>(BRANCA, pos);
  std::shared_ptr<Torre> torreBranca1 = std::make_shared<Torre>(BRANCA, posP1);
  std::shared_ptr<Torre> torreBranca2 = std::make_shared<Torre>(BRANCA, posP2);
  std::shared_ptr<Torre> torreBranca3 = std::make_shared<Torre>(BRANCA, posP3);
  std::shared_ptr<Torre> torreBranca4 = std::make_shared<Torre>(BRANCA, posP4);

  Tabuleiro *t = new Tabuleiro();
  t->casaDaPosicao(pos)->ocupar(torre);
  t->casaDaPosicao(posP1)->ocupar(torreBranca1);
  t->casaDaPosicao(posP2)->ocupar(torreBranca2);
  t->casaDaPosicao(posP3)->ocupar(torreBranca3);
  t->casaDaPosicao(posP4)->ocupar(torreBranca4);

  // chamadas consecutivas devem limpar o vetor
  torre->gerarMovimentos(t);
  torre->gerarMovimentos(t);
  vector<Movimento>& m = torre->gerarMovimentos(t);

  ASSERT_EQ(1, (int)m.size());
  ASSERT_EQ(1, std::count(m.begin(), m.end(), Movimento(pos, Posicao(posP4.linha(), posP4.coluna()-1), false)));

  delete t;
}