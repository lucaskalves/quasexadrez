#include "../posicao.h"
#include "gtest/gtest.h"

// teste do construtor padrao daposicao
TEST(PosicaoTest, ConstrucaoPadrao) {
  Posicao posPadrao;

  EXPECT_TRUE(posPadrao.linha() == 0);
  EXPECT_TRUE(posPadrao.coluna() == 0);
}

// teste do construtor com valores
TEST(PosicaoTest, ConstrucaoComValores) {
  Posicao pos = Posicao(3,2);

  EXPECT_TRUE(pos.linha() == 3);
  EXPECT_TRUE(pos.coluna() == 2);
}

// teste do metodo equal correto
TEST(PosicaoTest, EqualCorreto) {
  Posicao p1 = Posicao(1,2);
  Posicao p2 = Posicao(1,2);

  EXPECT_TRUE(p1.equal(p2));
}


// teste do metodo equal com posicoes diferentes
TEST(PosicaoTest, NotEqual) {
  Posicao p1 = Posicao(1,2);
  Posicao p2 = Posicao(4,5);

  EXPECT_TRUE(!p1.equal(p2));
}