cmake_minimum_required(VERSION 2.6)
set(PROJECT_NAME_STR quasexadrez)
set(SRC_DIR ../..)
set(TESTS_DIR ..)
set(COMMON_INCLUDES ../..)
set(GTEST_DIR ../gtest-1.6.0)

project(${PROJECT_NAME_STR})
add_definitions(-Wall -pthread -std=c++0x)
enable_testing()
add_subdirectory(${GTEST_DIR} ./gtest-1.6.0)

include_directories(${GTEST_DIR}/include ${COMMON_INCLUDES})

#######################################################
# ADICIONE AQUI AS CLASSES BASICAS QUE SERAO TESTADAS #
#######################################################

add_library(casa ${SRC_DIR}/casa.cpp)
add_library(posicao ${SRC_DIR}/posicao.cpp)
add_library(movimento ${SRC_DIR}/movimento.cpp)
add_library(peca ${SRC_DIR}/peca.cpp)
add_library(peao ${SRC_DIR}/peao.cpp)
add_library(torre ${SRC_DIR}/torre.cpp)
add_library(tabuleiro ${SRC_DIR}/tabuleiro.cpp)

#################################################
# ADICIONE AQUI OS TESTES QUE SERAO CONSTRUIDOS #
#################################################

## EXECUTAVEL ALLTESTS
set(TEST_SOURCES
      ${TESTS_DIR}/posicaotest.cpp
      ${TESTS_DIR}/movimentotest.cpp
      ${TESTS_DIR}/casatest.cpp
      ${TESTS_DIR}/peaotest.cpp
      ${TESTS_DIR}/torretest.cpp
      ${TESTS_DIR}/tabuleirotest.cpp)
add_executable(alltests ${TEST_SOURCES})

## DEPENDENCIAS DOS TESTES
target_link_libraries(alltests gtest gtest_main pthread)
target_link_libraries(alltests posicao)
target_link_libraries(alltests movimento)
target_link_libraries(alltests casa)
target_link_libraries(alltests tabuleiro)
target_link_libraries(alltests torre)
target_link_libraries(alltests peao)
target_link_libraries(alltests peca)
