#ifndef JOGADOR_H
#define JOGADOR_H

#include "peca.h"
#include "movimento.h"
#include "tabuleiro.h"

class Jogador {
public:
  virtual ~Jogador();
  virtual void definirCor(Cor c) = 0;
  virtual Cor cor() = 0;
  virtual Movimento fazerJogada(Tabuleiro &t) = 0;
};

#endif