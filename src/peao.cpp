#include "peao.h"


Peao::Peao(Cor cor, Posicao p) : Peca("peão", cor, PEAO, p) {
  if(!Tabuleiro::posicaoValida(p)) {
    throw 171;
  }

  m_FoiMovida = false;
  m_PosicaoInicial = p;
  m_Frente = (cor == PRETA) ? +1 : -1;}

Peao::~Peao() {
}

void Peao::moverParaPosicao(Posicao p) {
  if(!Tabuleiro::posicaoValida(p)) {
    throw 156;
  }

  m_Posicao = p;

  if(!m_Posicao.equal(m_PosicaoInicial)) {
    m_FoiMovida = true;
  } else {
    m_FoiMovida = false;
  }
}


bool Peao::movimentoValido(Posicao pn, Tabuleiro *t) {
  bool linhaValida;
  bool colunaValida;
  bool colunaValidaDireita;
  bool colunaValidaEsquerda;

  // se a nova posicao for invalida, o movimento ja eh proibido
  if(!Tabuleiro::posicaoValida(pn)) {
    return false;
  }

  // a peca deve ser movimentada
  if(m_Posicao.equal(pn)) {
    return false;
  }

  // um passo a frente em uma posicao livre
  if(t->posicaoLivre(pn)) {

    // é primeiro movimento -> pode dar um passo duplo
    if(!m_FoiMovida) {
      linhaValida = (pn.linha() == (m_Posicao.linha() + 2*m_Frente));
      colunaValida = (pn.coluna() == m_Posicao.coluna());

      if(linhaValida && colunaValida) {
        Posicao posIntermediaria = Posicao(m_Posicao.linha() + m_Frente, m_Posicao.coluna());
        bool posicaoIntermediariaLivre = t->posicaoLivre(posIntermediaria);

        if(posicaoIntermediariaLivre) {
          return true;
        }
      }

    }

    // avanca uma casa a frente
    linhaValida = pn.linha() == (m_Posicao.linha() + m_Frente);
    colunaValida = pn.coluna() == m_Posicao.coluna();

    if(linhaValida && colunaValida) {
      return true;
    }

    // confere 'en passant'
    // vai matar uma peca do adversario
    linhaValida = pn.linha() == (m_Posicao.linha() + m_Frente);
    colunaValidaDireita = pn.coluna() == m_Posicao.coluna() -1;
    colunaValidaEsquerda = pn.coluna() == m_Posicao.coluna() +1;

    if(linhaValida && (colunaValidaDireita || colunaValidaEsquerda)) {
      Posicao posAdversario = Posicao(m_Posicao.linha(), pn.coluna());
      Posicao posIniAdversario = Posicao(posAdversario.linha()+2*m_Frente,pn.coluna());

      if(t->houveJogada()) {
        if(t->ultimoMovimento() == Movimento(posIniAdversario,posAdversario, false)
          && !t->posicaoLivre(posAdversario)) {

          if(t->pecaEmPosicao(posAdversario)->cor() != m_Cor
              && t->pecaEmPosicao(posAdversario)->tipo() == PEAO
              && t->pecaMovida()->equal(*(t->pecaEmPosicao(posAdversario)))) {
            return true;
          }
        }
      }
    }

  } else {
    // vai matar uma peca do adversario
    linhaValida = pn.linha() == (m_Posicao.linha() + m_Frente);
    colunaValidaDireita = pn.coluna() == m_Posicao.coluna() -1;
    colunaValidaEsquerda = pn.coluna() == m_Posicao.coluna() +1;

    if(linhaValida && (colunaValidaDireita || colunaValidaEsquerda)
       && t->pecaEmPosicao(pn)->cor() != m_Cor) {
        // comida trivial
        return true;
    }
  }

  return false;
}

vector<Movimento>& Peao::gerarMovimentos(Tabuleiro *t) {
  // apaga os movimentos anteriores
  movimentos.clear();

  // Saida de peao pode avancar duas
  if(!m_FoiMovida) {
    int linhaFinal = m_Posicao.linha() + 2*m_Frente;
    Posicao posicaoAvancaDuas = Posicao(linhaFinal, m_Posicao.coluna());

    if(Tabuleiro::posicaoValida(posicaoAvancaDuas)) {

      if(movimentoValido(posicaoAvancaDuas, t)) {
        Movimento movimentoAvancaDuas = Movimento(m_Posicao, posicaoAvancaDuas, false);
        movimentos.push_back(movimentoAvancaDuas);
      }
    }
  }

  // Posicoes possiveis
  int posicoes[3][2];
  // avanca uma casa
  posicoes[0][0] = m_Posicao.linha() + m_Frente;
  posicoes[0][1] = m_Posicao.coluna();
  // come um peao a esquerda ou en passant
  posicoes[1][0] = m_Posicao.linha() + m_Frente;
  posicoes[1][1] = m_Posicao.coluna() - 1;
  // come um peao a direita ou en passant
  posicoes[2][0] = m_Posicao.linha() + m_Frente;
  posicoes[2][1] = m_Posicao.coluna() + 1;

  // dos movimenos possiveis, so adiciona os validos no vetor
  for(int i = 0; i<3; i++) {
    Posicao posicaoNova = Posicao(posicoes[i][0], posicoes[i][1]);
    
    if(Tabuleiro::posicaoValida(posicaoNova)) {

      if(movimentoValido(posicaoNova, t)) {
        bool vaiMatar = false;
        if(!t->posicaoLivre(posicaoNova) || m_Posicao.coluna() != posicaoNova.coluna()) {
          vaiMatar = true;
        }

        Movimento movimento = Movimento(m_Posicao, posicaoNova, vaiMatar);
        movimentos.push_back(movimento);
      }
    }
  }

  return movimentos;
}