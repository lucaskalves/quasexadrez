#ifndef JOGADORMAQUINA_H
#define JOGADORMAQUINA_H
#include "jogador.h"
#include "movimento.h"
#include "tabuleiro.h"

class JogadorMaquina: public Jogador {
  Cor m_Cor;
  int m_ProfundidadeMaxima;

  int negamax(Tabuleiro &estado, int profundidade, int alpha, int beta, int maximizar);
  Movimento melhorMovimento;
  
public:
  JogadorMaquina(int profundidadeMaxima);
  ~JogadorMaquina();
  void definirCor(Cor c);
  Cor cor();
  void defineProfundidade(int p);
  Movimento fazerJogada(Tabuleiro &t);
};

#endif
