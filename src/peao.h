#ifndef PEAO_H
#define PEAO_H

#include <string>
#include "peca.h"
#include "posicao.h"
#include "tabuleiro.h"
#include "movimento.h"

class Peao: public Peca
{
private:
  bool m_FoiMovida;
  int m_Frente;
  Posicao m_PosicaoInicial;

public:
  Peao(Cor cor, Posicao p);
  ~Peao();
  void moverParaPosicao(Posicao p);
  bool movimentoValido(Posicao pn, Tabuleiro *t);
  vector<Movimento>& gerarMovimentos(Tabuleiro *t);
};

#endif