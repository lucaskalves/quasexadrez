#include "jogadorhumano.h"

JogadorHumano::JogadorHumano() {
}

JogadorHumano::~JogadorHumano() {

}

void JogadorHumano::definirCor(Cor c) {
  m_Cor = c;
}

Cor JogadorHumano::cor() {
  return m_Cor;
}

Movimento JogadorHumano::fazerJogada(Tabuleiro &t) {
  // usa interface para ver o que o usuario quer
  return Movimento(Posicao(0,0),Posicao(0,0), false);
}
