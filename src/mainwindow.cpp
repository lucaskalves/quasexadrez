#include <QMouseEvent>
#include <QMessageBox>
#include <QString>

#include <iostream>
#include <memory>

#include "mainwindow.h"
#include "posicao.h"
#include "peca.h"


/**
* Essa classe controla a interface do jogo e faz todo o controle dele.
* As jogadas são gerenciadas por essa classe. Ela eh responsavel
* por chamar a jogada da maquina e tratar as entradas humanas.
*/
MainWindow::MainWindow(QWidget *parent)
: QWidget(parent)
{

  m_ClicouEmCasa = false;
  ui.setupUi(this);

  installaFiltrosDeEvento();
}

void MainWindow::definirTabuleiro(Tabuleiro *t) {
  m_Tabuleiro = t;
}

void MainWindow::definirPartida(Partida *partida) {
  m_Partida = partida;
}


void MainWindow::defineJogadorMaquina(JogadorMaquina* jm){
    m_JogadorMaquina=jm;
}

// evento do botao de iniciar a partida do jogo
void MainWindow::on_comecarPararButton_clicked() {
  std::cout<<"começou"<<std::endl;
  imprimirTabuleiro();
  qApp->processEvents();
  m_Partida->iniciar();

  if(m_Partida->vezDaMaquina()) {
    gerenciaFimDeJogada();
  }

  ui.comecarPararButton->setEnabled(false);
  ui.trocarCoresButton->setEnabled(false);
}

// botao de trocar cores dos jogadores
void MainWindow::on_trocarCoresButton_clicked() {
  m_Partida->trocarCores();

  QString nomeCorMaq = QString((m_Partida->jogadorMaquina()->cor()==BRANCA) ? "Branca" : "Preta");
  ui.corComputadorLabel->setText(nomeCorMaq);
  QString nomeCorHum = QString((m_Partida->jogadorHumano()->cor()==BRANCA) ? "Branca" : "Preta");
  ui.corHumanoLabel->setText(nomeCorHum);
}

// controle de eventos nas casas do tabuleiro da interface
void MainWindow::handlerDeClickEmCasa(QLabel* f, Posicao p) {
  if(m_Partida->foiIniciada()) {
    if(m_Partida->jogadorAtual() == m_Partida->jogadorHumano()) {
      if(!m_ClicouEmCasa) {
        // primeiro click
        if(!m_Tabuleiro->posicaoLivre(p)) {
          m_PrimeiroClick = f;
          m_PosicaoInicial = p;
          m_ClicouEmCasa = true;
          marcarPosicao(m_PosicaoInicial);
        }
      } else {
        // segundo click
        m_SegundoClick = f;
        m_PosicaoFinal = p;
        marcarPosicao(p);
        validaMovimento();

        m_ClicouEmCasa = false;
        desmarcarPosicao(m_PosicaoInicial);
        desmarcarPosicao(m_PosicaoFinal);
      }
    }
  }
}

// verifica se a jogada do humano eh valida e executa ela.
void MainWindow::validaMovimento() {

  if(!m_Tabuleiro->posicaoLivre(m_PosicaoInicial)) {
    std::shared_ptr<Peca> peca = m_Tabuleiro->pecaEmPosicao(m_PosicaoInicial);

    if(peca->cor() == m_Partida->jogadorHumano()->cor() && peca->movimentoValido(m_PosicaoFinal, m_Tabuleiro)) {
      bool vaiMatar = false;
      if(!m_Tabuleiro->posicaoLivre(m_PosicaoFinal)) {
        vaiMatar = true;
      }
      if(m_Tabuleiro->posicaoLivre(m_PosicaoFinal)
        && m_Tabuleiro->pecaEmPosicao(m_PosicaoInicial)->tipo() == PEAO
        && m_PosicaoInicial.coluna() != m_PosicaoFinal.coluna()) {
        // deve ser enpassant
        vaiMatar = true;
      }
      Movimento mov = Movimento(m_PosicaoInicial,m_PosicaoFinal, vaiMatar);

      desmarcarPosicao(m_Tabuleiro->ultimoMovimento().posicaoInicial());
      desmarcarPosicao(m_Tabuleiro->ultimoMovimento().posicaoFinal());
      m_Tabuleiro->mover(mov);
      imprimirTabuleiro();
      imprimirMovimento(mov);
      m_MovimentoLido = true;
      m_Partida->trocarVezJogador();
      qApp->processEvents();
      gerenciaFimDeJogada();
    }
  }
}

// gerencia o final da jogada do humano, verificando fim de jogo e chama a maquina
// para executar a jogada
void MainWindow::gerenciaFimDeJogada() {
  if( !m_Partida->fimDeJogo() && m_Partida->jogadorAtual() != m_Partida->jogadorHumano()) {
    try {
      Movimento mov = m_Partida->executaMaquina();
      desmarcarPosicao(m_Tabuleiro->ultimoMovimento().posicaoInicial());
      desmarcarPosicao(m_Tabuleiro->ultimoMovimento().posicaoFinal());
      imprimirTabuleiro();
      imprimirMovimento(mov);
    } catch(int e) {
      QMessageBox errorMsgbox;
      QString errrorMessage;

      errrorMessage = QString::fromStdString("Erro na execucao da maquina. Numero do erro: ");
      errrorMessage += QString::number(e);
      errorMsgbox.setWindowTitle("Um erro terrivel aconteceu...");
      errorMsgbox.setText(errrorMessage);
      errorMsgbox.exec();
    }
    
    m_Partida->trocarVezJogador();
  }

  if(m_Partida->fimDeJogo()) {
    QMessageBox Msgbox;

    QString vencedor;
    if(m_Partida->vencedor()->cor() == BRANCA) {
      vencedor = QString::fromStdString("Branca");
    } else {
      vencedor = QString::fromStdString("Preta");
    }

    Msgbox.setText(QString::fromStdString("Jogo acabou! Vencedor: ") + vencedor);
    Msgbox.exec();
  }
}

void MainWindow::imprimirMovimento(Movimento mov) {
  marcarPosicao(mov.posicaoInicial());
  marcarPosicao(mov.posicaoFinal());
}

// mudanca no botao de profundiadade
void MainWindow::on_profundidadeInput_valueChanged(int value)
{
    m_JogadorMaquina->defineProfundidade(value);
}

void MainWindow::marcarPosicao(Posicao p) {

  std::shared_ptr<Casa> c = m_Tabuleiro->casaDaPosicao(p);
  QLabel* f = frameDaPosicao(p);
  if(c->estaOcupada()) {
    if(c->peca()->cor() == PRETA && c->peca()->tipo() == PEAO) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_black.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(166, 32, 0);"));

    } else if(c->peca()->cor() == PRETA && c->peca()->tipo() == TORRE) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_black.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(166, 32, 0);"));

    } else if(c->peca()->cor() == BRANCA && c->peca()->tipo() == PEAO) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_white.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(166, 32, 0);"));

    } else if(c->peca()->cor() == BRANCA && c->peca()->tipo() == TORRE) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_white.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(166, 32, 0);"));
    }
  } else {
    f->setStyleSheet(QLatin1String("background-position: center center;\n"
      "background-repeat: no-repeat;\n"
      "border: 0;\n"
      "margin: 0;\n"
      "background-color: rgb(166, 32, 0);"));
  }

}

void MainWindow::desmarcarPosicao(Posicao p) {
  std::shared_ptr<Casa> c = m_Tabuleiro->casaDaPosicao(p);
  QLabel* f = frameDaPosicao(p);

  if(c->estaOcupada()) {
    if(c->peca()->cor() == PRETA && c->peca()->tipo() == PEAO) {
      if(c->corDaCasa() == CASABRANCA) {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_black.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 255, 255);"));
      } else {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_black.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 199, 87);"));
      }

    } else if(c->peca()->cor() == PRETA && c->peca()->tipo() == TORRE) {
      if(c->corDaCasa() == CASABRANCA) {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_black.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 255, 255);"));
      } else {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_black.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 199, 87);"));
      }

    } else if(c->peca()->cor() == BRANCA && c->peca()->tipo() == PEAO) {
      if(c->corDaCasa() == CASABRANCA) {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_white.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 255, 255);"));
      } else {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_white.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 199, 87);"));
      }

    } else if(c->peca()->cor() == BRANCA && c->peca()->tipo() == TORRE) {
      if(c->corDaCasa() == CASABRANCA) {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_white.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 255, 255);"));
      } else {
        f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_white.svg);\n"
          "background-position: center center;\n"
          "background-repeat: no-repeat;\n"
          "border: 0;\n"
          "margin: 0;\n"
          "background-color: rgb(255, 199, 87);"));
      }
    }
  } else {
     if(c->corDaCasa() == CASABRANCA) {
      f->setStyleSheet(QLatin1String("background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 255, 255);"));
    } else {
      f->setStyleSheet(QLatin1String("background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 199, 87);"));
    }
  }
}

void MainWindow::esvaziarPosicao(Posicao p) {
  std::shared_ptr<Casa> c = m_Tabuleiro->casaDaPosicao(p);
  QLabel* f = frameDaPosicao(p);

  if(c->corDaCasa() == CASABRANCA) {
    f->setStyleSheet(QLatin1String("background-position: center center;\n"
      "background-repeat: no-repeat;\n"
      "border: 0;\n"
      "margin: 0;\n"
      "background-color: rgb(255, 255, 255);"));
  } else {
    f->setStyleSheet(QLatin1String("background-position: center center;\n"
      "background-repeat: no-repeat;\n"
      "border: 0;\n"
      "margin: 0;\n"
      "background-color: rgb(255, 199, 87);"));
  }
}

void MainWindow::imprimirTabuleiro() {
  Posicao posicao;
  for(int i=0; i < Tabuleiro::LINHAS; i++) {
    for(int j=0; j < Tabuleiro::COLUNAS; j++) {
      posicao = Posicao(i,j);

      if(!m_Tabuleiro->posicaoLivre(posicao)) {
        QLabel* frame = frameDaPosicao(posicao);
        imprimirPeca(frame, m_Tabuleiro->casaDaPosicao(posicao));
      } else {
        esvaziarPosicao(posicao);
      }
    }
  }
}

void MainWindow::imprimirPeca(QLabel* f, std::shared_ptr<Casa>c) {
  if(c->peca()->cor() == PRETA && c->peca()->tipo() == PEAO) {
    if(c->corDaCasa() == CASABRANCA) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_black.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 255, 255);"));
    } else {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_black.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 199, 87);"));
    }

  } else if(c->peca()->cor() == PRETA && c->peca()->tipo() == TORRE) {
    if(c->corDaCasa() == CASABRANCA) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_black.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 255, 255);"));
    } else {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_black.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 199, 87);"));
    }

  } else if(c->peca()->cor() == BRANCA && c->peca()->tipo() == PEAO) {
    if(c->corDaCasa() == CASABRANCA) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_white.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 255, 255);"));
    } else {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/pawn_white.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 199, 87);"));
    }

  } else if(c->peca()->cor() == BRANCA && c->peca()->tipo() == TORRE) {
    if(c->corDaCasa() == CASABRANCA) {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_white.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 255, 255);"));
    } else {
      f->setStyleSheet(QLatin1String("background-image: url(:/images/rook_white.svg);\n"
        "background-position: center center;\n"
        "background-repeat: no-repeat;\n"
        "border: 0;\n"
        "margin: 0;\n"
        "background-color: rgb(255, 199, 87);"));
    }
  }
}

void MainWindow::installaFiltrosDeEvento() {
  ui.casa_a1->installEventFilter( this );
  ui.casa_a2->installEventFilter( this );
  ui.casa_a3->installEventFilter( this );
  ui.casa_a4->installEventFilter( this );
  ui.casa_a5->installEventFilter( this );
  ui.casa_a6->installEventFilter( this );
  ui.casa_a7->installEventFilter( this );
  ui.casa_a8->installEventFilter( this );
  ui.casa_b1->installEventFilter( this );
  ui.casa_b2->installEventFilter( this );
  ui.casa_b3->installEventFilter( this );
  ui.casa_b4->installEventFilter( this );
  ui.casa_b5->installEventFilter( this );
  ui.casa_b6->installEventFilter( this );
  ui.casa_b7->installEventFilter( this );
  ui.casa_b8->installEventFilter( this );
  ui.casa_c1->installEventFilter( this );
  ui.casa_c2->installEventFilter( this );
  ui.casa_c3->installEventFilter( this );
  ui.casa_c4->installEventFilter( this );
  ui.casa_c5->installEventFilter( this );
  ui.casa_c6->installEventFilter( this );
  ui.casa_c7->installEventFilter( this );
  ui.casa_c8->installEventFilter( this );
  ui.casa_d1->installEventFilter( this );
  ui.casa_d2->installEventFilter( this );
  ui.casa_d3->installEventFilter( this );
  ui.casa_d4->installEventFilter( this );
  ui.casa_d5->installEventFilter( this );
  ui.casa_d6->installEventFilter( this );
  ui.casa_d7->installEventFilter( this );
  ui.casa_d8->installEventFilter( this );
  ui.casa_e1->installEventFilter( this );
  ui.casa_e2->installEventFilter( this );
  ui.casa_e3->installEventFilter( this );
  ui.casa_e4->installEventFilter( this );
  ui.casa_e5->installEventFilter( this );
  ui.casa_e6->installEventFilter( this );
  ui.casa_e7->installEventFilter( this );
  ui.casa_e8->installEventFilter( this );
  ui.casa_f1->installEventFilter( this );
  ui.casa_f2->installEventFilter( this );
  ui.casa_f3->installEventFilter( this );
  ui.casa_f4->installEventFilter( this );
  ui.casa_f5->installEventFilter( this );
  ui.casa_f6->installEventFilter( this );
  ui.casa_f7->installEventFilter( this );
  ui.casa_f8->installEventFilter( this );
  ui.casa_g1->installEventFilter( this );
  ui.casa_g2->installEventFilter( this );
  ui.casa_g3->installEventFilter( this );
  ui.casa_g4->installEventFilter( this );
  ui.casa_g5->installEventFilter( this );
  ui.casa_g6->installEventFilter( this );
  ui.casa_g7->installEventFilter( this );
  ui.casa_g8->installEventFilter( this );
  ui.casa_h1->installEventFilter( this );
  ui.casa_h2->installEventFilter( this );
  ui.casa_h3->installEventFilter( this );
  ui.casa_h4->installEventFilter( this );
  ui.casa_h5->installEventFilter( this );
  ui.casa_h6->installEventFilter( this );
  ui.casa_h7->installEventFilter( this );
  ui.casa_h8->installEventFilter( this );
}

bool MainWindow::eventFilter( QObject *dist, QEvent *event )
{
  if( event->type() == QEvent::MouseButtonPress )
  {
    if(dist == ui.casa_a1) {
      handlerDeClickEmCasa(ui.casa_a1, Posicao(0, 0));
    } else if(dist == ui.casa_a2) {
      handlerDeClickEmCasa(ui.casa_a2, Posicao(1, 0));
    } else if(dist == ui.casa_a3) {
      handlerDeClickEmCasa(ui.casa_a3, Posicao(2, 0));
    } else if(dist == ui.casa_a4) {
      handlerDeClickEmCasa(ui.casa_a4, Posicao(3, 0));
    } else if(dist == ui.casa_a5) {
      handlerDeClickEmCasa(ui.casa_a5, Posicao(4, 0));
    } else if(dist == ui.casa_a6) {
      handlerDeClickEmCasa(ui.casa_a6, Posicao(5, 0));
    } else if(dist == ui.casa_a7) {
      handlerDeClickEmCasa(ui.casa_a7, Posicao(6, 0));
    } else if(dist == ui.casa_a8) {
      handlerDeClickEmCasa(ui.casa_a8, Posicao(7, 0));
    } else if(dist == ui.casa_b1) {
      handlerDeClickEmCasa(ui.casa_b1, Posicao(0, 1));
    } else if(dist == ui.casa_b2) {
      handlerDeClickEmCasa(ui.casa_b2, Posicao(1, 1));
    } else if(dist == ui.casa_b3) {
      handlerDeClickEmCasa(ui.casa_b3, Posicao(2, 1));
    } else if(dist == ui.casa_b4) {
      handlerDeClickEmCasa(ui.casa_b4, Posicao(3, 1));
    } else if(dist == ui.casa_b5) {
      handlerDeClickEmCasa(ui.casa_b5, Posicao(4, 1));
    } else if(dist == ui.casa_b6) {
      handlerDeClickEmCasa(ui.casa_b6, Posicao(5, 1));
    } else if(dist == ui.casa_b7) {
      handlerDeClickEmCasa(ui.casa_b7, Posicao(6, 1));
    } else if(dist == ui.casa_b8) {
      handlerDeClickEmCasa(ui.casa_b8, Posicao(7, 1));
    } else if(dist == ui.casa_c1) {
      handlerDeClickEmCasa(ui.casa_c1, Posicao(0, 2));
    } else if(dist == ui.casa_c2) {
      handlerDeClickEmCasa(ui.casa_c2, Posicao(1, 2));
    } else if(dist == ui.casa_c3) {
      handlerDeClickEmCasa(ui.casa_c3, Posicao(2, 2));
    } else if(dist == ui.casa_c4) {
      handlerDeClickEmCasa(ui.casa_c4, Posicao(3, 2));
    } else if(dist == ui.casa_c5) {
      handlerDeClickEmCasa(ui.casa_c5, Posicao(4, 2));
    } else if(dist == ui.casa_c6) {
      handlerDeClickEmCasa(ui.casa_c6, Posicao(5, 2));
    } else if(dist == ui.casa_c7) {
      handlerDeClickEmCasa(ui.casa_c7, Posicao(6, 2));
    } else if(dist == ui.casa_c8) {
      handlerDeClickEmCasa(ui.casa_c8, Posicao(7, 2));
    } else if(dist == ui.casa_d1) {
      handlerDeClickEmCasa(ui.casa_d1, Posicao(0, 3));
    } else if(dist == ui.casa_d2) {
      handlerDeClickEmCasa(ui.casa_d2, Posicao(1, 3));
    } else if(dist == ui.casa_d3) {
      handlerDeClickEmCasa(ui.casa_d3, Posicao(2, 3));
    } else if(dist == ui.casa_d4) {
      handlerDeClickEmCasa(ui.casa_d4, Posicao(3, 3));
    } else if(dist == ui.casa_d5) {
      handlerDeClickEmCasa(ui.casa_d5, Posicao(4, 3));
    } else if(dist == ui.casa_d6) {
      handlerDeClickEmCasa(ui.casa_d6, Posicao(5, 3));
    } else if(dist == ui.casa_d7) {
      handlerDeClickEmCasa(ui.casa_d7, Posicao(6, 3));
    } else if(dist == ui.casa_d8) {
      handlerDeClickEmCasa(ui.casa_d8, Posicao(7, 3));
    } else if(dist == ui.casa_e1) {
      handlerDeClickEmCasa(ui.casa_e1, Posicao(0, 4));
    } else if(dist == ui.casa_e2) {
      handlerDeClickEmCasa(ui.casa_e2, Posicao(1, 4));
    } else if(dist == ui.casa_e3) {
      handlerDeClickEmCasa(ui.casa_e3, Posicao(2, 4));
    } else if(dist == ui.casa_e4) {
      handlerDeClickEmCasa(ui.casa_e4, Posicao(3, 4));
    } else if(dist == ui.casa_e5) {
      handlerDeClickEmCasa(ui.casa_e5, Posicao(4, 4));
    } else if(dist == ui.casa_e6) {
      handlerDeClickEmCasa(ui.casa_e6, Posicao(5, 4));
    } else if(dist == ui.casa_e7) {
      handlerDeClickEmCasa(ui.casa_e7, Posicao(6, 4));
    } else if(dist == ui.casa_e8) {
      handlerDeClickEmCasa(ui.casa_e8, Posicao(7, 4));
    } else if(dist == ui.casa_f1) {
      handlerDeClickEmCasa(ui.casa_f1, Posicao(0, 5));
    } else if(dist == ui.casa_f2) {
      handlerDeClickEmCasa(ui.casa_f2, Posicao(1, 5));
    } else if(dist == ui.casa_f3) {
      handlerDeClickEmCasa(ui.casa_f3, Posicao(2, 5));
    } else if(dist == ui.casa_f4) {
      handlerDeClickEmCasa(ui.casa_f4, Posicao(3, 5));
    } else if(dist == ui.casa_f5) {
      handlerDeClickEmCasa(ui.casa_f5, Posicao(4, 5));
    } else if(dist == ui.casa_f6) {
      handlerDeClickEmCasa(ui.casa_f6, Posicao(5, 5));
    } else if(dist == ui.casa_f7) {
      handlerDeClickEmCasa(ui.casa_f7, Posicao(6, 5));
    } else if(dist == ui.casa_f8) {
      handlerDeClickEmCasa(ui.casa_f8, Posicao(7, 5));
    } else if(dist == ui.casa_g1) {
      handlerDeClickEmCasa(ui.casa_g1, Posicao(0, 6));
    } else if(dist == ui.casa_g2) {
      handlerDeClickEmCasa(ui.casa_g2, Posicao(1, 6));
    } else if(dist == ui.casa_g3) {
      handlerDeClickEmCasa(ui.casa_g3, Posicao(2, 6));
    } else if(dist == ui.casa_g4) {
      handlerDeClickEmCasa(ui.casa_g4, Posicao(3, 6));
    } else if(dist == ui.casa_g5) {
      handlerDeClickEmCasa(ui.casa_g5, Posicao(4, 6));
    } else if(dist == ui.casa_g6) {
      handlerDeClickEmCasa(ui.casa_g6, Posicao(5, 6));
    } else if(dist == ui.casa_g7) {
      handlerDeClickEmCasa(ui.casa_g7, Posicao(6, 6));
    } else if(dist == ui.casa_g8) {
      handlerDeClickEmCasa(ui.casa_g8, Posicao(7, 6));
    } else if(dist == ui.casa_h1) {
      handlerDeClickEmCasa(ui.casa_h1, Posicao(0, 7));
    } else if(dist == ui.casa_h2) {
      handlerDeClickEmCasa(ui.casa_h2, Posicao(1, 7));
    } else if(dist == ui.casa_h3) {
      handlerDeClickEmCasa(ui.casa_h3, Posicao(2, 7));
    } else if(dist == ui.casa_h4) {
      handlerDeClickEmCasa(ui.casa_h4, Posicao(3, 7));
    } else if(dist == ui.casa_h5) {
      handlerDeClickEmCasa(ui.casa_h5, Posicao(4, 7));
    } else if(dist == ui.casa_h6) {
      handlerDeClickEmCasa(ui.casa_h6, Posicao(5, 7));
    } else if(dist == ui.casa_h7) {
      handlerDeClickEmCasa(ui.casa_h7, Posicao(6, 7));
    } else if(dist == ui.casa_h8) {
      handlerDeClickEmCasa(ui.casa_h8, Posicao(7, 7));
    }

    return true;
  }

  return false;
}


QLabel* MainWindow::frameDaPosicao(Posicao p) {

  if(p.linha() == 0) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a1;
      break;
      case 1:
      return ui.casa_b1;
      break;
      case 2:
      return ui.casa_c1;
      break;
      case 3:
      return ui.casa_d1;
      break;
      case 4:
      return ui.casa_e1;
      break;
      case 5:
      return ui.casa_f1;
      break;
      case 6:
      return ui.casa_g1;
      break;
      case 7:
      return ui.casa_h1;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 1) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a2;
      break;
      case 1:
      return ui.casa_b2;
      break;
      case 2:
      return ui.casa_c2;
      break;
      case 3:
      return ui.casa_d2;
      break;
      case 4:
      return ui.casa_e2;
      break;
      case 5:
      return ui.casa_f2;
      break;
      case 6:
      return ui.casa_g2;
      break;
      case 7:
      return ui.casa_h2;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 2) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a3;
      break;
      case 1:
      return ui.casa_b3;
      break;
      case 2:
      return ui.casa_c3;
      break;
      case 3:
      return ui.casa_d3;
      break;
      case 4:
      return ui.casa_e3;
      break;
      case 5:
      return ui.casa_f3;
      break;
      case 6:
      return ui.casa_g3;
      break;
      case 7:
      return ui.casa_h3;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 3) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a4;
      break;
      case 1:
      return ui.casa_b4;
      break;
      case 2:
      return ui.casa_c4;
      break;
      case 3:
      return ui.casa_d4;
      break;
      case 4:
      return ui.casa_e4;
      break;
      case 5:
      return ui.casa_f4;
      break;
      case 6:
      return ui.casa_g4;
      break;
      case 7:
      return ui.casa_h4;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 4) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a5;
      break;
      case 1:
      return ui.casa_b5;
      break;
      case 2:
      return ui.casa_c5;
      break;
      case 3:
      return ui.casa_d5;
      break;
      case 4:
      return ui.casa_e5;
      break;
      case 5:
      return ui.casa_f5;
      break;
      case 6:
      return ui.casa_g5;
      break;
      case 7:
      return ui.casa_h5;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 5) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a6;
      break;
      case 1:
      return ui.casa_b6;
      break;
      case 2:
      return ui.casa_c6;
      break;
      case 3:
      return ui.casa_d6;
      break;
      case 4:
      return ui.casa_e6;
      break;
      case 5:
      return ui.casa_f6;
      break;
      case 6:
      return ui.casa_g6;
      break;
      case 7:
      return ui.casa_h6;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 6) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a7;
      break;
      case 1:
      return ui.casa_b7;
      break;
      case 2:
      return ui.casa_c7;
      break;
      case 3:
      return ui.casa_d7;
      break;
      case 4:
      return ui.casa_e7;
      break;
      case 5:
      return ui.casa_f7;
      break;
      case 6:
      return ui.casa_g7;
      break;
      case 7:
      return ui.casa_h7;
      break;
      default:
      return NULL;
      break;
    }

  } else if(p.linha() == 7) {
    switch(p.coluna()) {
      case 0:
      return ui.casa_a8;
      break;
      case 1:
      return ui.casa_b8;
      break;
      case 2:
      return ui.casa_c8;
      break;
      case 3:
      return ui.casa_d8;
      break;
      case 4:
      return ui.casa_e8;
      break;
      case 5:
      return ui.casa_f8;
      break;
      case 6:
      return ui.casa_g8;
      break;
      case 7:
      return ui.casa_h8;
      break;
      default:
      return NULL;
      break;
    }
  }

  return NULL;
}
