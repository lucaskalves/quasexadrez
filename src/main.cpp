#include <QApplication>

#include "mainwindow.h"
#include "tabuleiro.h"
#include "partida.h"
#include "jogador.h"
#include "jogadorhumano.h"
#include "jogadormaquina.h"


/**
* Programa principal o jogo. Inicia a interface e o controle da partida.
*/
int main(int argc, char *argv[])
{
  // incializa tabuleiro e partida com esse tabuleiro
  Tabuleiro tabuleiro = Tabuleiro();
  tabuleiro.montarTabuleiroInicial();
  Partida partida = Partida(tabuleiro);

  // inicializa jogadores
  JogadorHumano jogadorBranco = JogadorHumano();
  jogadorBranco.definirCor(BRANCA);
  JogadorMaquina jogadorPreto = JogadorMaquina(5);
  jogadorPreto.definirCor(PRETA);

  // define os jogadores basicos (pode ser alterado atraves da interface)
  // em tempo de execucao.
  partida.definirJogadorBranco(&jogadorBranco);
  partida.definirJogadorHumano(BRANCA);
  partida.definirJogadorPreto(&jogadorPreto);

  // inicializa a aplicacao Qt
  QApplication app(argc, argv);

  // inicializa a classe que controla a logica do jogo
  MainWindow mainwindow;
  mainwindow.defineJogadorMaquina(&jogadorPreto);
  mainwindow.definirTabuleiro(&tabuleiro);
  mainwindow.definirPartida(&partida);
  mainwindow.show();

  return app.exec();
}
