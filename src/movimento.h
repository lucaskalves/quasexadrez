#ifndef MOVIMENTO_H
#define MOVIMENTO_H

#include "posicao.h"
#include <iostream>

class Movimento {
  Posicao m_PosicaoInicial;
  Posicao m_PosicaoFinal;
  bool m_MataPeca;

public:
  Movimento();
  Movimento(Posicao pi, Posicao pf, bool mataPeca);
  Posicao posicaoInicial() const;
  Posicao posicaoFinal() const;
  bool mataPeca() const;
  bool operator== (const Movimento &o) const;
  bool operator!= (const Movimento &o) const;
};

std::ostream& operator<<(std::ostream& os, const Movimento& movimento);

#endif