#include "peca.h"
#include <stdio.h>

Peca::~Peca() {}

std::string Peca::nome() const {
  return m_Nome;
}

Cor Peca::cor() const {
  return m_Cor;
}

Tipo Peca::tipo() const {
  return m_Tipo;
}

Posicao Peca::posicao() const {
  return m_Posicao;
}

bool Peca::equal(const Peca &o) const {
  bool mesmoNome = (nome().compare(o.nome()) == 0);
  bool mesmaCor = cor() == o.cor();
  bool mesmoTipo = tipo() == o.tipo();
  bool mesmaPosicao = posicao().equal(o.posicao());

  return mesmoNome && mesmaCor && mesmoTipo && mesmaPosicao;
}
