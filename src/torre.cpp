#include "torre.h"
#include <stdio.h>


Torre::Torre(Cor cor, Posicao p) : Peca("torre", cor, TORRE, p) {
  if(!Tabuleiro::posicaoValida(p)) {
    throw "tentando criar peca com posicao invalida";
  }
}

void Torre::moverParaPosicao(Posicao p) {
  if(!Tabuleiro::posicaoValida(p)) {
    throw "tentando mover peca para posicao invalida";
  }

  m_Posicao = p;
}

bool Torre::movimentoValido(Posicao pn, Tabuleiro *t){
  // se a nova posicao for invalida, o movimento ja eh proibido
  if(!Tabuleiro::posicaoValida(pn)) {
    return false;
  }

  // a peca deve ser movimentada
  if(m_Posicao.equal(pn)) {
    return false;
  }

  // se o local destino tiver uma peca de mesma cor o movimento eh proibido
  if(!t->posicaoLivre(pn)) {
    if(t->pecaEmPosicao(pn)->cor() == m_Cor) {
      return false;
    }
  }

  // checa se o movimento é vertical ou horizontal
  if(m_Posicao.coluna() == pn.coluna()) {
    
    // sentido do movimento
    int passo;
    if(m_Posicao.linha() < pn.linha()) {
      passo = +1;
    } else {
      passo = -1;
    }

    // verifica se ha pecas no meio do caminho
    int linha = m_Posicao.linha() + passo;
    int coluna = m_Posicao.coluna();
    bool valido = true;
    while(valido && linha != pn.linha()) {
      Posicao posIntermediaria = Posicao(linha, coluna);

      if(!t->posicaoLivre(posIntermediaria)) {
        valido = false;
      }

      linha = linha + passo;
    }

    return valido;
  
  } else if(m_Posicao.linha() == pn.linha()) {

    // sentido do movimento
    int passo;
    if(m_Posicao.coluna() < pn.coluna()) {
      passo = +1;
    } else {
      passo = -1;
    }

    // verifica se ha pecas no meio do caminho
    int linha = m_Posicao.linha();
    int coluna = m_Posicao.coluna() + passo;
    bool valido = true;
    while(valido && coluna != pn.coluna()) {
      Posicao posIntermediaria = Posicao(linha, coluna);

      if(!t->posicaoLivre(posIntermediaria)) {
        valido = false;
      }

      coluna = coluna + passo;
    }
    
    return valido;
  }

  return false;
}

vector<Movimento>& Torre::gerarMovimentos(Tabuleiro *t) {
  int i, j;
  Posicao posFinal;

  // libera os movimentos antigos
  movimentos.clear();

  // gera os movimentos verticais subindo o tabuleiro
  i = m_Posicao.linha() + 1;
  posFinal = Posicao(i, m_Posicao.coluna());
  
  while(i<Tabuleiro::LINHAS && movimentoValido(posFinal, t)) {
    bool vaiMatar = false;
    if(!t->posicaoLivre(posFinal)) {
      vaiMatar = true;
    }

    Movimento movimento = Movimento(m_Posicao, posFinal, vaiMatar);
    movimentos.push_back(movimento);
    
    i += 1;
    posFinal = Posicao(i, m_Posicao.coluna());
  }

  // gera os movimentos verticais descendo o tabuleiro
  i = m_Posicao.linha() - 1;
  posFinal = Posicao(i, m_Posicao.coluna());
  
  while(i>=0 && movimentoValido(posFinal, t)) {
    bool vaiMatar = false;
    if(!t->posicaoLivre(posFinal)) {
      vaiMatar = true;
    }

    Movimento movimento = Movimento(m_Posicao, posFinal, vaiMatar);
    movimentos.push_back(movimento);
    
    i -= 1;
    posFinal = Posicao(i, m_Posicao.coluna());
  }

  // gera os movimentos horizontais indo para a direita do tabuleiro
  j = m_Posicao.coluna() + 1;
  posFinal = Posicao(m_Posicao.linha(), j);
  
  while(j<Tabuleiro::COLUNAS && movimentoValido(posFinal, t)) {
    bool vaiMatar = false;
    if(!t->posicaoLivre(posFinal)) {
      vaiMatar = true;
    }

    Movimento movimento = Movimento(m_Posicao, posFinal, vaiMatar);
    movimentos.push_back(movimento);
    
    j += 1;
    posFinal = Posicao(m_Posicao.linha(), j);
  }

  // gera os movimentos horizontais indo para a esquerda do tabuleiro
  j = m_Posicao.coluna() - 1;
  posFinal = Posicao(m_Posicao.linha(), j);
  
  while(j>=0 && movimentoValido(posFinal, t)) {
    bool vaiMatar = false;
    if(!t->posicaoLivre(posFinal)) {
      vaiMatar = true;
    }

    Movimento movimento = Movimento(m_Posicao, posFinal, vaiMatar);
    movimentos.push_back(movimento);
    
    j -= 1;
    posFinal = Posicao(m_Posicao.linha(), j);
  }

  return movimentos;
}