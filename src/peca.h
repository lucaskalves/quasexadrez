#ifndef PECA_H
#define PECA_H

enum Cor {BRANCA, PRETA};
enum Tipo {PEAO, TORRE};

#include <string>
#include <vector>
#include "posicao.h"
#include "tabuleiro.h"
#include "movimento.h"

class Tabuleiro;

using std::vector;

class Peca
{
protected:
  std::string m_Nome;
  Cor m_Cor;
  Tipo m_Tipo;
  Posicao m_Posicao;

public:
  vector<Movimento> movimentos;

  Peca(std::string n, Cor c, Tipo t, Posicao p) :
        m_Nome(n), m_Cor(c), m_Tipo(t), m_Posicao(p){};
  virtual ~Peca();

  std::string nome() const;
  Cor cor() const;
  Tipo tipo() const;
  Posicao posicao() const;
  bool equal(const Peca&) const;
  virtual void moverParaPosicao(Posicao p) = 0;
  virtual bool movimentoValido(Posicao pn, Tabuleiro *t) = 0;
  virtual vector<Movimento>& gerarMovimentos(Tabuleiro *t) = 0;
};

#endif