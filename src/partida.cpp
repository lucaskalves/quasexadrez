#include "partida.h"
#include "peca.h"
#include <stdio.h>

/**
* Representa a partida atual, com cores dos jogadores, vencedor, 
* execao e maquina.
*/
Partida::Partida(Tabuleiro &t) : m_Tabuleiro(t) {
  m_JogadorPreto = 0;
  m_JogadorBranco = 0;
  m_JogadorAtual = 0;
  m_CorHumano = BRANCA;
  m_FoiIniciada = false;
}

// inicia a partida (jogador branco sempre comeca)
void Partida::iniciar() {
  m_FoiIniciada = true;
  m_JogadorAtual = m_JogadorBranco;
}

bool Partida::foiIniciada() {
  return m_FoiIniciada;
}

void Partida::definirJogadorBranco(Jogador *j) {
  m_JogadorBranco = j;
}

void Partida::definirJogadorPreto(Jogador *j) {
  m_JogadorPreto = j;
}

void Partida::definirJogadorHumano(Cor cor) {
  m_CorHumano = cor;
}

void Partida::trocarCores() {
  m_JogadorBranco->definirCor(PRETA);
  m_JogadorPreto->definirCor(BRANCA);
  
  Jogador *temp = m_JogadorBranco;
  m_JogadorBranco = m_JogadorPreto;
  m_JogadorPreto = temp;

  m_CorHumano = (m_CorHumano == BRANCA) ? PRETA : BRANCA;
}

Jogador *Partida::jogadorAtual() {
  return m_JogadorAtual;
}

Jogador *Partida::jogadorHumano() {
  if(m_CorHumano == BRANCA) {
    return m_JogadorBranco;
  } else {
    return m_JogadorPreto;
  }
}

Jogador *Partida::jogadorMaquina() {
  if(m_CorHumano == BRANCA) {
    return m_JogadorPreto;
  } else {
    return m_JogadorBranco;
  }
}

void Partida::trocarVezJogador() {
  if(m_JogadorAtual == m_JogadorBranco) {
    m_JogadorAtual = m_JogadorPreto;
  } else {
    m_JogadorAtual = m_JogadorBranco;
  }
}

bool Partida::vezDaMaquina() {
  return m_JogadorAtual != jogadorHumano();
}

bool Partida::vezDoHumano() {
  return m_JogadorAtual == jogadorHumano();
}

Movimento Partida::executaMaquina() {
  Movimento mov = m_JogadorAtual->fazerJogada(m_Tabuleiro);
  m_Tabuleiro.mover(mov);
  return mov;
}

bool Partida::fimDeJogo(){
  return m_Tabuleiro.fimDeJogo();
}

Jogador *Partida::vencedor() {
  return m_Tabuleiro.corVencedora() == BRANCA ? m_JogadorBranco : m_JogadorPreto;
}