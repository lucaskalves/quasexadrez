#ifndef POSICAO_H
#define POSICAO_H

#include <iostream>

class Posicao {
  int m_Linha;
  int m_Coluna;

public:
  Posicao();
  Posicao(int l, int c);

  int linha() const;
  int coluna() const;
  bool equal(Posicao o);
};

std::ostream& operator<<(std::ostream& os, const Posicao& posicao);

#endif