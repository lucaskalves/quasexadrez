#ifndef MAINFORM_H
#define MAINFORM_H

#include "../ui/ui_mainwindow.h"
#include "tabuleiro.h"
#include "casa.h"
#include "movimento.h"
#include "posicao.h"
#include "partida.h"
#include "jogador.h"
#include "jogadorhumano.h"
#include "jogadormaquina.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    void definirTabuleiro(Tabuleiro *t);
    void definirPartida(Partida *partida);
    void imprimirTabuleiro();
    void defineJogadorMaquina(JogadorMaquina* jm);
private slots:
    void on_profundidadeInput_valueChanged(int value);
    void on_comecarPararButton_clicked();
    void on_trocarCoresButton_clicked();

protected:
  bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::MainWindow ui;
    QLabel* frameDaPosicao(Posicao p);
    void imprimirPeca(QLabel* f, std::shared_ptr<Casa>c);
    void validaMovimento();
    void esvaziarPosicao(Posicao p);
    void imprimirMovimento(Movimento mov);
    void handlerDeClickEmCasa(QLabel* f, Posicao p);
    void installaFiltrosDeEvento();
    void gerenciaFimDeJogada();
    void marcarPosicao(Posicao p);
    void desmarcarPosicao(Posicao p);

    Partida *m_Partida;
    Tabuleiro *m_Tabuleiro;
    bool m_ClicouEmCasa;
    QLabel* m_PrimeiroClick;
    QLabel* m_SegundoClick;
    Posicao m_PosicaoInicial;
    Posicao m_PosicaoFinal;
    JogadorMaquina* m_JogadorMaquina;
    bool m_MovimentoLido;
};

#endif
